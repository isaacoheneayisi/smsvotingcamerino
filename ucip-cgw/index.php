<?php
// you might need to replace some paramenters in the xml, msisdn, amount, datetime
// billing xml

$msisdn = trim($_GET['msisdn']);
$transID = trim($_GET['transID']);

$now = new DateTime();
$cur =  $now->format("Ymd\TH:i:sO");

$xml = '<?xml version="1.0"?>
<methodCall>
<methodName>UpdateBalanceAndDate</methodName>
<params>
<param>
<value>
<struct>
<member><name>originHostName</name><value><string>node01</string></value></member>
<member><name>adjustmentAmountRelative</name><value>-10</value></member>
<member><name>originNodeType</name><value><string>EXT</string></value></member>
<member><name>originTransactionID</name><value><string>'.$transID.'</string></value></member>
<member><name>originTimeStamp</name><value><dateTime.iso8601>'.$cur.'</dateTime.iso8601></value></member>
<member><name>transactionCurrency</name><value><string>GHC</string></value></member>
<member><name>subscriberNumber</name><value><string>'.$msisdn.'</string></value></member>
</struct>
</value>
</param>
</params>
</methodCall>';


# check balance xml
/*
$xml = '<?xml version="1.0"?>
<methodCall>
<methodName>GetBalanceAndDate</methodName>
<params>
<param>
<value>
<struct>
<member><name>originHostName</name><value><string>elcuto</string></value></member>
<member><name>originNodeType</name><value><string>EXT</string></value></member>
<member><name>originTransactionID</name><value><string>2</string></value></member>
<member><name>originTimeStamp</name><value><dateTime.iso8601>20160413T03:40:26+0200</dateTime.iso8601></value></member>
<member><name>transactionCurrency</name><value><string>GHC</string></value></member>
<member><name>subscriberNumber</name><value><string>269001234</string></value></member>
</struct>
</value>
</param>
</params>
</methodCall>
';
*/



// put the actual charging url
$url = "http://10.93.64.22:10010/Air";
$xml_length = strlen($xml);

$headers = buildheaders($url, $xml_length);

$response = sendxml($url, $xml, $headers);

header("Content-type: text/xml");
// place the log file path here
$file = 'billing_xml.log';
$data = "\n=========================================================================\n";
file_put_contents($file, $data . $xml, FILE_APPEND);

echo $response;


function sendxml($url, $xml_post_string, $headers)
{
    
    
    $soapUser = "mtech3"; // replace with the actual user if any and if NOT comment line 55 out
    $soapPassword = "mtech3"; // replace with the actual password if any and if NOT comment line 55 out

    $ch = curl_init();
    // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_USERPWD, $soapUser . ":" . $soapPassword);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    // curl_setopt($ch, CURLOPT_KEYPASSWD, "12345678");
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    // you can uncomment it and put the real path to .crt file if give.
    // curl_setopt($ch, CURLOPT_CAINFO, '/opt/nginx/html/kenya_ws/TWSSCG.crt');

    // converting
    $response = curl_exec($ch);
    curl_close($ch);

    return $response;
}
function buildheaders($soapUrl, $xml_length)
{
    $headers = array(
        "POST /Air HTTP/1.1",
        "User-Agent:UGw Server/3.1/1.0",
        "Authorization: Basic b25tb2JpbGU6b25tb2JpbGU=",
        "Content-Type:text/xml",
        "Accept: text/xml",
        "Cache-Control: no-cache",
        "SOAPAction: " . $soapUrl,
        "Pragma: no-cache",
        "Content-length: " . $xml_length);
    return $headers;
}
?>

