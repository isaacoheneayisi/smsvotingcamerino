       <div class="page-footer">
            <div class="page-footer-inner"> 
			2016 &copy; MTECH GHANA                
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>

        <!-- END FOOTER -->
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
		
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
		   
        <script src="assets/global/plugins/moment.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
        <!-- <script src="assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script> -->
        <!-- <script src="assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
        <script src="assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
        <script src="assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
        <script src="assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
        <script src="assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
        <script src="assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
        <script src="assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
        <script src="assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
        <script src="assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
        <script src="assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script> -->
        <script src="assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <!-- <script src="assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script> -->
        <script src="assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <!-- <script src="assets/pages/scripts/dashboard.min.js" type="text/javascript"></script> -->
        <!-- END PAGE LEVEL SCRIPTS -->
		 <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="assets/pages/scripts/table-datatables-colreorder.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
		<!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
		<!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="assets/pages/scripts/form-validation.min.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL SCRIPTS -->
		<!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <!-- <script src="assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
        <script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script> -->
        <script src="assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
    <?php if (@$page_title == "Home") :?>
        <script src="assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
        <script src="assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
        <script src="assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>   
        <script src="assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>    
         <script src="assets/pages/scripts/view_transport.js" type="text/javascript"></script>
    <?php endif; ?>
    <?php if (@$page_title == "Sent Items") :?>
        <script src="assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
        <script src="assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
        <script src="assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>   
        <script src="assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>    
         <script src="assets/pages/scripts/view_sent_items.js" type="text/javascript"></script>
    <?php endif; ?>


    <?php if (@$page_title == "Report") :?>
        <script type="text/javascript" src="assets/global/scripts/datatable.js"></script>
        <script type="text/javascript" src="assets/global/plugins/datatables/datatables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.11/api/sum().js"></script>
        <script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"></script>
        <script src="assets/pages/scripts/reports.js" type="text/javascript"></script>
    <?php endif; ?>

    <?php if (@$page_title == "Admin Report") :?>
        <script type="text/javascript" src="assets/global/scripts/datatable.js"></script>
        <script type="text/javascript" src="assets/global/plugins/datatables/datatables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.11/api/sum().js"></script>
        <script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"></script>
        <script src="assets/pages/scripts/adminreport.js" type="text/javascript"></script>
    <?php endif; ?>

    <?php if (@$page_title == "view partners") :?>
        <!-- <script type="text/javascript" src="assets/global/scripts/datatable.js"></script> -->
        <script type="text/javascript" src="assets/global/plugins/datatables/datatables.min.js"></script>
        <script src="assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="assets/global/plugins/bootbox/bootbox.min.js"></script>
        <script src="assets/pages/scripts/view-partners.js" type="text/javascript"></script>
    <?php endif; ?>
    <?php if (@$page_title == "blast summary") :?>
        <script src="assets/pages/scripts/table-blast-summary.js" type="text/javascript"></script>
    <?php endif; ?>
    <?php if (@$page_title == "airtel blast summary") :?>
        <script src="assets/pages/scripts/table-airtel-blast-summary.js" type="text/javascript"></script>
    <?php endif; ?>
    
    <?php if (@$page_title == "all partners") :?>
        <script src="assets/pages/scripts/table-all-partners.js" type="text/javascript"></script>
    <?php endif; ?>

    <?php if (@$page_title == "Add Transport") :?>
        <script src="assets/pages/scripts/transport.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
    <?php endif; ?>

    <?php if (@$page_title == "Add candidates") :?>
        <script src="assets/pages/scripts/addcandidates.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
    <?php endif; ?>
    <?php if (@$page_title == "Add students") :?>
        <script src="assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
        <script src="assets/pages/scripts/addstudents.js" type="text/javascript"></script>
        
    <?php endif; ?>

     

    <?php if (@$page_title == "Add User") :?>
        <script src="assets/pages/scripts/manage_user.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
    <?php endif; ?>

    <?php if (@$page_title == "tisu report") :?>
        <script src="assets/pages/scripts/table-view-tisu.js" type="text/javascript"></script>
    <?php endif; ?>

    <?php if(@$is_post_form): ?>
        <script src="assets/global/plugins/jquery.blockui.min.js"></script>
        <script src="assets/global/plugins/jquery.textareaCounter.plugin.js"></script>
        <script src="assets/global/scripts/manage_post.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
    <?php endif; ?>

        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
        <script src="assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->


    </body>
   
</html>