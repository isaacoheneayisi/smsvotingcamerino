
                      <?php  
                        $page_title = "Add students";
                         $page_menu ="Third Party";
                         $sub_page_title = "Party Details";
                         $section = "Admin";
                         
					     include_once('includes/config.php');
                         include_once('includes/func.php');
                         include ("header.php");
                         if($_SESSION['access_level'] != "5"){
                            header('location:index.php');

                        }  					        
					   ?>
                        
                          <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                
                    <?php include_once('includes/breadcrumbs/breadcrumbs.php'); ?>
                 
                    <div class="clearfix"></div>
                    <!-- END DASHBOARD STATS 1-->
                            <!-- Main content page-->
                         <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN VALIDATION STATES-->
                            <div class="portlet light portlet-fit portlet-form bordered ">
                                <pre>
                                    
                                    
                                </pre>
                                <div class="portlet-body">
                                    <!-- BEGIN FORM-->
                                    <form id="add_students" autocomplete="false" class="form-horizontal">
                                    <input type="hidden" name="opera" value="add_students"/>
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                                           <!-- <div class="form-group">
                                                <label class="control-label col-md-3">Candidates Name
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="candidate_name" id="company_name" data-required="1" class="form-control" /> </div>
                                            </div>  -->     
                                               <div class="form-group">
                                                <label class="control-label col-md-3"> School Name 
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <select name="school" id="company" class="form-control">
                                            <?php 
                                            $val = get_company_names();
                                            while($row = pg_fetch_array($val)) { ?>
                                            <option value="<?php echo $row[0] ?>"><?php echo $row[1]; ?></option>
                                            <?php } ?>                                                
                                                
                                            </select>

                                                     </div>
                                            </div>   

											<!--  <div class="form-group">
                                                <label class="control-label col-md-3">CONTACT PERSON'S LAST NAME
                                                    <span class="required"> </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="lname" id="lname" data-required="1" class="form-control" /> </div>
                                            </div>  -->   
											<!-- <div class="form-group">
                                                <label class="control-label col-md-3">CONTACT EMAIL
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="email" name="email" id="email" data-required="1" class="form-control" /> </div>
                                            </div>  -->
                                          <!--  <div class="form-group">
                                                <label class="control-label col-md-3">PHONE 1
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="tel" name="phone1" id="phone1" data-required="1" class="form-control" /> </div>
                                            </div>->

                                          <!--  <div class="form-group">
                                                <label class="control-label col-md-3">PHONE 2
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="tel" name="phone2" id="phone2" data-required="1" class="form-control" /> </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">SMS SERVICE
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <select class="form-control" id="account_type" name="account_type" required>
                                                        <option value="pay">Post Paid</option>
                                                        <option value="unlimited">Pay Monthly</option>                                                  
                                                        
                                                    </select>
                                                </div>
                                            </div>
											 
                                             <div class="form-group">
                                                <label class="control-label col-md-3">COST/SMS
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="cost" id="cost" data-required="1" class="form-control" /> </div>
                                            </div>-->

											 <!--<div class="form-group">
                                                <label class="control-label col-md-3">LOGIN
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="xxname" id="xxname" data-required="1" class="form-control" /> </div>
                                            </div>  
											 <div class="form-group">
                                                <label class="control-label col-md-3">PASSWORD
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="password" name="xxpass" id="xxpass" data-required="1" class="form-control" /> </div>
                                            </div>  
											 <div class="form-group">
                                                <label class="control-label col-md-3">CONFIRM PASSWORD
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="password" name="confirm_xxpass" id="confirm_xxpass" data-required="1" class="form-control" /> </div>
                                            </div>  

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Active
                                                    <span class="required">  </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="checkbox" name="active" value="1" /> </div>
                                            </div>  -->

											                           
                                           
                                        <div class="form-actions">

                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn green">Submit</button>
                                                    <button type="button" class="btn default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM
                                </div>
                                <!-- END VALIDATION STATES-->
                            </div>
                        </div>
                    </div>
                </div>
                            <!-- end main content page-->
                      
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
      
	                      <?php   
					  
					        include ("footer.php");
					   ?>