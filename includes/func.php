<?php
/**
 * Created by PhpStorm.
 * User: Gideon Paitoo
 * Date: 4/13/2016
 * Time: 12:27 PM
 */
include_once('config.php');
$conn = pg_connect(LOCAL_CONN );
if(!isset($_SESSION)){
    session_start();
}
//$accountid = $_SESSION['accountid'];
// uncomment this code $access = $_SESSION['access_level'];

if(isset($_REQUEST['action']) && !empty($_REQUEST['action'])) {
    $action = $_REQUEST['action'];
    switch($action) {
        case 'getchart' : get_data_for_graph();break;
        case 'getreport' : get_reports();break;
        // ...etc...
    }
}
// get service owners
function log_sms($apikey,$source,$destination,$msg,$extra,$response_code){
    $msg = base64_encode($msg);
    global $conn;
    $sql = "INSERT INTO smslog(owner,source,destination,msg,extra,response_code,smswhen) VALUES('$apikey','$source','$destination','$msg','$extra','$response_code','NOW()')";
    pg_query($conn,$sql);
}

function check_student_ID($student_ID){

    global $conn;

    $sql = "select students_id from students where students_id ilike '%$student_ID%'";
    $results =pg_query($conn,$sql);
    $rows = pg_num_rows($results);
    if ($rows > 0){
        return 1;
    }else{
        return 0;
    }
}

function check_If_Voted($student_ID){

    global $conn;

    $sql = "select student_id from votes where student_id ilike '%$student_ID%'";
    $results =pg_query($conn,$sql);
    $rows = pg_num_rows($results);
    if ($rows > 0){
        return 1;
    }else{
        return 0;
    }
}

function expected_votes(){

    global $conn;
    $Total=0;
    $sql = "select count(id) from students;";
    $results =pg_query($conn,$sql);
   while($row = pg_fetch_row( $results)) {
    $Total = $row[0];
   }
   return $Total;
}

function successful_votes(){

    global $conn;
    $Total=0;
    $sql = "select count(id) from votes where action like '%success%'";
    $results =pg_query($conn,$sql);
   while($row = pg_fetch_row( $results)) {
    $Total = $row[0];
   }
   return $Total;
}

function cancelled_votes(){

    global $conn;
    $Total=0;
    $sql = "select count(id) from votes where action like '%already voted%'";
    $results =pg_query($conn,$sql);
   while($row = pg_fetch_row( $results)) {
    $Total = $row[0];
   }
   return $Total;
}

function john_votes(){

    global $conn;
    $Total=0;
    $sql = "select count(id) from votes where action like '%success%' and text_recorded ilike '%john%'";
    $results =pg_query($conn,$sql);
   while($row = pg_fetch_row( $results)) {
    $Total = $row[0];
   }
   return $Total;
}

function isaac_votes(){

    global $conn;
    $Total=0;
    $sql = "select count(id) from votes where action like '%success%' and text_recorded ilike '%isaac%'";
    $results =pg_query($conn,$sql);
   while($row = pg_fetch_row( $results)) {
    $Total = $row[0];
   }
   return $Total;
}

function mary_votes(){

    global $conn;
    $Total=0;
    $sql = "select count(id) from votes where action like '%success%' and text_recorded ilike '%mary%'";
    $results =pg_query($conn,$sql);
   while($row = pg_fetch_row( $results)) {
    $Total = $row[0];
   }
   return $Total;
}

function esther_votes(){

    global $conn;
    $Total=0;
    $sql = "select count(id) from votes where action like '%success%' and text_recorded ilike '%esther%'";
    $results =pg_query($conn,$sql);
   while($row = pg_fetch_row( $results)) {
    $Total = $row[0];
   }
   return $Total;
}



function insert_votes ($student_id,$phone_numbers,$text_recorded){
    global $conn;
    $action="success";
    $sql = "INSERT INTO votes(school_id,student_id,action,phone_numbers,date_recorded,text_recorded) VALUES(3,'$student_id','$action','$phone_numbers',NOW(),'$text_recorded')";
    pg_query($conn,$sql);
}

function insert_already_voted ($student_id,$phone_numbers,$text_recorded){
    global $conn;
    $action="already voted";
    $sql = "INSERT INTO votes(school_id,student_id,action,phone_numbers,date_recorded,text_recorded) VALUES(3,'$student_id','$action','$phone_numbers',NOW(),'$text_recorded')";
    pg_query($conn,$sql);
}

function sendSMS($to,$response)
{

$text = urlencode($response);

//$to = str_replace("+", "", $number);
// calling sendsm url
$url= "http://10.0.0.247:13013/cgi-bin/sendsms?username=tester&password=foobar&from=393510990256&to=".$to."&text=".$text."&smsc=393510990256";
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$output = curl_exec($ch);        
curl_close($ch);

}

function get_total_sms(){
    global $conn;
    global $accountid;
    global $access;
    if($access == '5'){
        $sql = "SELECT count(id) FROM smslog WHERE date_trunc('month',smswhen) = date_trunc('month',NOW())";
    }else{
        $sql = "SELECT count(id) FROM smslog WHERE date_trunc('month',smswhen) = date_trunc('month',NOW()) AND smsowner = '$accountid'";
    }
    
    $result = pg_query($conn,$sql);
    $r = pg_fetch_array($result);
    return $r['count'];
}

function get_account_balance(){
   /* global $conn;
    global $accountid;
    $sql = "SELECT credit FROM account WHERE apikey = '$accountid'";
    $result = pg_query($conn,$sql);
    $r = pg_fetch_array($result);
    return $r['credit'];*/
}

function get_total_success(){
    global $conn;
    global $accountid;
    global $access;
    if($access == '5'){
        $sql = "SELECT count(id) FROM smslog WHERE date_trunc('month',smswhen) = date_trunc('month',NOW()) AND response_code = 0";
    }else{
        $sql = "SELECT count(id) FROM smslog WHERE date_trunc('month',smswhen) = date_trunc('month',NOW()) AND response_code = 0 AND smsowner = '$accountid'";
    }
    
    $result = pg_query($conn,$sql);
    $r = pg_fetch_array($result);
    return $r['count'];
}

function get_total_fail(){
    global $conn;
    global $accountid;
    global $access;
    if($access == '5'){
        $sql = "SELECT count(id) FROM smslog WHERE date_trunc('month',smswhen) = date_trunc('month',NOW()) AND response_code != 0";
    }else{
        $sql = "SELECT count(id) FROM smslog WHERE date_trunc('month',smswhen) = date_trunc('month',NOW()) AND response_code != 0 AND smsowner = '$accountid'";
    }
    $sql = "SELECT count(id) FROM smslog WHERE date_trunc('month',smswhen) = date_trunc('month',NOW()) AND response_code != 0 AND smsowner = '$accountid'";
    $result = pg_query($conn,$sql);
    $r = pg_fetch_array($result);
    return $r['count'];
}

function get_data_for_graph(){
    global $conn;
    //global $accountid;
   // global $access;
   // if($access == '5' || $access == '1'){
        $sql="select distinct(text_recorded), count(id) as total_cont from votes where action='success' group by text_recorded";

    /*$sql = "SELECT to_char(now() + interval '-6' day, 'day') AS day1,count(id) AS  c1 FROM smslog WHERE date_trunc('day',smswhen) = date_trunc('day',now() + interval '-6' day) 
    UNION ALL 
    SELECT to_char(now() + interval '-5' day, 'day') AS day1,count(id) AS  c1 FROM smslog WHERE date_trunc('day',smswhen) = date_trunc('day',now() + interval '-5' day) 
    UNION ALL 
    SELECT to_char(now() + interval '-4' day, 'day') AS day1,count(id) AS  c1 FROM smslog WHERE date_trunc('day',smswhen) = date_trunc('day',now() + interval '-4' day) 
    UNION ALL 
    SELECT to_char(now() + interval '-3' day, 'day') AS day1,count(id) AS  c1 FROM smslog WHERE date_trunc('day',smswhen) = date_trunc('day',now() + interval '-3' day) 
    UNION ALL 
    SELECT to_char(now() + interval '-2' day, 'day') AS day1,count(id) AS  c1 FROM smslog WHERE date_trunc('day',smswhen) = date_trunc('day',now() + interval '-2' day) 
    UNION ALL 
    SELECT to_char(now() + interval '-1' day, 'day') AS day1,count(id) AS  c1 FROM smslog WHERE date_trunc('day',smswhen) = date_trunc('day',now() + interval '-1' day) 
    UNION ALL 
    SELECT to_char(now(), 'day') AS day1,count(id) AS  c1 FROM smslog WHERE date_trunc('day',smswhen) = date_trunc('day',now())";
}else{
    $sql = "SELECT to_char(now() + interval '-6' day, 'day') AS day1,count(id) AS  c1 FROM smslog WHERE date_trunc('day',smswhen) = date_trunc('day',now() + interval '-6' day) AND smsowner = '$accountid'
    UNION ALL 
    SELECT to_char(now() + interval '-5' day, 'day') AS day1,count(id) AS  c1 FROM smslog WHERE date_trunc('day',smswhen) = date_trunc('day',now() + interval '-5' day) AND smsowner = '$accountid'
    UNION ALL 
    SELECT to_char(now() + interval '-4' day, 'day') AS day1,count(id) AS  c1 FROM smslog WHERE date_trunc('day',smswhen) = date_trunc('day',now() + interval '-4' day) AND smsowner = '$accountid'
    UNION ALL 
    SELECT to_char(now() + interval '-3' day, 'day') AS day1,count(id) AS  c1 FROM smslog WHERE date_trunc('day',smswhen) = date_trunc('day',now() + interval '-3' day) AND smsowner = '$accountid'
    UNION ALL 
    SELECT to_char(now() + interval '-2' day, 'day') AS day1,count(id) AS  c1 FROM smslog WHERE date_trunc('day',smswhen) = date_trunc('day',now() + interval '-2' day) AND smsowner = '$accountid'
    UNION ALL 
    SELECT to_char(now() + interval '-1' day, 'day') AS day1,count(id) AS  c1 FROM smslog WHERE date_trunc('day',smswhen) = date_trunc('day',now() + interval '-1' day) AND smsowner = '$accountid'
    UNION ALL 
    SELECT to_char(now(), 'day') AS day1,count(id) AS  c1 FROM smslog WHERE date_trunc('day',smswhen) = date_trunc('day',now()) AND smsowner = '$accountid'
";*/
//}

$result = pg_query($conn,$sql);
while($r = pg_fetch_array($result)){
    $items[] = array(
        "day" => trim($r['text_recorded']),
        "count" => $r['total_cont'],
        "color" => rand_color()
        );
}
echo json_encode($items);

}

function get_reports(){
    global $conn;
    $sql = "SELECT to_char(date_trunc('day',smswhen),'DD-Mon-YYYY') as smswhen, count(id) as allsms,
(select count(id) from smslog where response_code = 0 AND date_trunc('day',smswhen) = date_trunc('day','2016-05-02'::date))as successsms,
(select count(id) from smslog where response_code != 0 AND date_trunc('day',smswhen) = date_trunc('day','2016-05-02'::date)) as failsms 
FROM smslog WHERE date_trunc('day',smswhen) = date_trunc('day','2016-05-02'::date) 
GROUP BY smswhen";
$result = pg_query($conn,$sql);

$items = array(
        "sEcho" => 1,
        "iTotalRecords" => pg_num_rows($result),
        "iTotalDisplayRecords" => pg_num_rows($result),
          "aaData"=>null);

while($r = pg_fetch_array($result)){
        $items[] = array(
            "Date" => $r['smswhen'],
            "Total SMS" => $r['allsms'],
            "Success" => $r['successsms'],
            "Fail" => $r['failsms']         

            );
    }
    echo json_encode($items);
}

function get_partners($id) {
    global $conn;
    $sql = "SELECT * FROM account WHERE id = $id";
    //echo $sql;
    $result = pg_query ($conn, $sql);
    //$row = pg_fetch_all( $result );
    return $result;
}

function get_company_names() {
    global $conn;
    //$sql = "SELECT id,apikey FROM account";
    $sql = "SELECT id,school_name FROM schools";
    //echo $sql;
    $result = pg_query ($conn, $sql);
    //$row = pg_fetch_all( $result );
    return $result;
}
/*
function get_school_id($name) {
    global $conn;
    //$sql = "SELECT id,apikey FROM account";
    $sql = "SELECT id FROM schools where school_name ilike '%$name%'";
    //echo $sql;
    $result = pg_query ($conn, $sql);
    //$row = pg_fetch_all( $result );
    return $result;
}*/



function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
}

function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}

function contains($needle, $haystack)
{
    return strpos($haystack, $needle) !== false;
}

function rand_color() {
    return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
}