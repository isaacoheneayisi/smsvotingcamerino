
<div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                        <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                        <li class="sidebar-toggler-wrapper hide">
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <div class="sidebar-toggler"> </div>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                        </li>


        
    <?php if ($_SESSION['access_level'] == "5" || $_SESSION['access_level'] == "1") :?>

                        <li class="nav-item start ">
                            <a href="index.php" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">Home</span>
                                <!-- <span class="selected"></span> -->
                                <!-- <span class="arrow open"></span> -->
                            </a>                            
                        </li>

<?php if ($_SESSION['access_level'] == "1") :?>
                        <li class="nav-item  ">
                            <a href="report.php" class="nav-link nav-toggle">
                                <i class="icon-diamond"></i>
                                <span class="title">Reports</span>
                                <!-- <span class="arrow"></span> -->
                            </a>
                        </li>
<?php endif; ?>

    <?php if ($_SESSION['access_level'] == "1") :?>
                        <li class="nav-item  ">
                            <a href="change_password.php" class="nav-link nav-toggle">
                                <i class="icon-diamond"></i>
                                <span class="title">Change Password</span>
                                <!-- <span class="arrow"></span> -->
                            </a>
                        </li>
<?php endif; ?>
<?php endif; ?>
<!-- <?php if ($_SESSION['access_level'] == "555") :?>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-puzzle"></i>
                                <span class="title">SUPPORT</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="moblast.php" class="nav-link ">
                                        <span class="title">MO SMS BLAST</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="bulk_unsubscribe.php" class="nav-link ">
                                        <span class="title">BULK UNSUBSCRIBE</span>                                        
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="bulk_subscribe.php" class="nav-link ">
                                        <span class="title">BULK SUBSCRIBE</span>
                                    </a>
                                </li>
                               
                               
                            </ul>
                        </li>
<?php endif; ?>

<?php if ($_SESSION['access_level'] == "555" ) :?>
						 <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-puzzle"></i>
                                <span class="title">TRIVIA</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="viewtriviacontents.php" class="nav-link ">
                                        <span class="title">ALL TRIVIA</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="addtrivia.php" class="nav-link ">
                                        <span class="title">ADD NEW TRIVIA CONTENT</span>                                        
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="importtrivia.php" class="nav-link ">
                                        <span class="title">IMPORT TRIVIA</span>
                                    </a>
                                </li>
                                
                               
                            </ul>
                        </li>
<?php endif; ?>

<?php if ($_SESSION['access_level'] == "555" ) :?>
						 <li class="nav-item <?php if ( $section == "services" ) {echo "active open";} ?> ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-puzzle"></i>
                                <span class="title">SERVICES</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?php if ( $page_title == "view services" ) {echo "active";} ?> ">
                                    <a href="viewservices.php" class="nav-link ">
                                        <span class="title">VIEW SERVICES</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="addservice.php" class="nav-link ">
                                        <span class="title">ADD SERVICE</span>                                        
                                    </a>
                                </li>                              
                               
                            </ul>
                        </li>
<?php endif; ?>

<?php if ($_SESSION['access_level'] == "555" ) :?>					
						 <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-puzzle"></i>
                                <span class="title">PARTNERS/THIRD PARTY</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="allpartners.php" class="nav-link ">
                                        <span class="title">ALL PARTNERS DETAILS</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="add_thirdparty.php" class="nav-link ">
                                        <span class="title">ADD PARTNER</span>                                        
                                    </a>
                                </li>                              
                               
                            </ul>
                        </li>
<?php endif; ?>

<?php if ($_SESSION['access_level'] == "555" || $_SESSION['access_level'] == "333" ) :?>  
						 <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-puzzle"></i>
                                <span class="title">REPORT & STATISTICS</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                
								<li class="nav-item  ">
                                    <a href="airtel-blast_summary.php" class="nav-link ">
                                        <span class="title">AIRTEL BLAST SUMMARY</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="blast_summary.php" class="nav-link ">
                                        <span class="title">BLAST SUMMARY</span>
                                    </a>
                                </li>
								<li class="nav-item  ">
                                    <a href="instant_report.php" class="nav-link ">
                                        <span class="title">INSTANT REPORT</span>
                                    </a>
                                </li>
								<li class="nav-item  ">
                                    <a href="schedule_report.php" class="nav-link ">
                                        <span class="title">SCHEDULE REPORT</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="tisu_report.php" class="nav-link ">
                                        <span class="title">TISU REPORT</span>
                                    </a>
                                </li>
                                 
                                 <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-puzzle"></i>
                                <span class="title">MO REPORT</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="trivia_report.php" class="nav-link ">
                                        <span class="title">TRIVIA</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="voting_report.php" class="nav-link ">
                                        <span class="title">VOTING</span>                                        
                                    </a>
                                </li>                              
                               
                            </ul>
                        </li>							
                               
                            </ul>
                        </li>
						
<?php endif; ?>

-->
<?php if ($_SESSION['access_level'] == "5" ) :?>  
						<li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle <?php if ( $section == "Admin" ) {echo "active open";} ?>">
                                <i class="icon-users"></i>
                                <span class="title">ADMIN</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">                           

                                   <li class="nav-item  ">
                            <a href="viewpartners.php" class="nav-link nav-toggle">
                                <i class="icon-puzzle"></i>
                                <span class="title">Transaction</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                             <li class="nav-item  ">
                                    <a href="addpartner.php" class="nav-link ">
                                        <span class="title">Add Item</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="sent_items.php" class="nav-link ">
                                        <span class="title">Received Items</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="index.php" class="nav-link ">
                                        <span class="title">Sent Items</span>
                                    </a>
                                </li>
                               
                               <!-- <li class="nav-item  ">
                                    <a href="addcandidates.php" class="nav-link ">
                                        <span class="title">Add Candidates</span>
                                    </a>
                                </li>
                                 <li class="nav-item  ">
                                    <a href="addstudents.php" class="nav-link ">
                                        <span class="title">Add Students</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="adminreport.php" class="nav-link ">
                                        <span class="title">Reports</span>                                        
                                    </a>
                                </li>-->


                            </ul>
                        </li>			
<?php endif; ?>

<!--
<?php if ($_SESSION['access_level'] == "555" ) :?>                       
					   <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-puzzle"></i>
                                <span class="title">REPORT & STATISTICS</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="mt_report.php" class="nav-link ">
                                        <span class="title">MT REPORT</span>
                                    </a>
                                </li>
                                 
                                 <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-puzzle"></i>
                                <span class="title">MO REPORT</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="trivia_report.php" class="nav-link ">
                                        <span class="title">TRIVIA</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="voting_report.php" class="nav-link ">
                                        <span class="title">VOTING</span>                                        
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="handler_report.php" class="nav-link ">
                                        <span class="title">HANDLER REPORTS</span>
                                    </a>
                                </li>
                            </ul>
                        </li>


<?php endif; ?> 

<?php if ($_SESSION['access_level'] == "555" ) :?> 								
                               
                            </ul>
                        </li>
							<li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-puzzle"></i>
                                <span class="title">BLACKLIST</span>
                                <span class="arrow"></span>
                            </a>
                           
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="trivia_report.php" class="nav-link ">
                                        <span class="title">ADD BLACKLIST</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="voting_report.php" class="nav-link ">
                                        <span class="title">VIEW BLASTED NUMBERS</span>                                        
                                    </a>
                                </li>                              
                               
                            </ul>
                        </li>
<?php endif; ?> 

<?php if ($_SESSION['access_level'] == "555" ) :?> 

                         <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-puzzle"></i>
                                <span class="title">USERS</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="adduser.php" class="nav-link ">
                                        <span class="title">ADD USERS</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="viewusers.php" class="nav-link ">
                                        <span class="title">VIEW USERS</span>                                        
                                    </a>
                                </li>                              
                               
                            </ul>
                        </li>


								
                               
                            </ul>
                        </li>
<?php endif; ?>	 -->						
								
								
								<li class="nav-item  ">
                                    <a href="index.php?islogout=true" class="nav-link ">
                                    <i class="icon-logout"></i>
                                        <span class="title">LOGOUT<span>                                      
                                    </a>
                                </li> 
                            </ul>
                        </li>

                            </ul>
                        </li>
                    </ul>
                    <!-- END SIDEBAR MENU -->
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>