<?php

/**
 * Created by PhpStorm.
 * User: Gideon Paitoo
 * Date: 4/12/2016
 * Time: 10:28 AM
 */
session_start();
$fname = $_SESSION['firstname'];
include_once('config.php');
include_once('func.php');
$conn = pg_connect(LOCAL_CONN );

if ($conn) {
    $conn_status = "Connection Ok";
} else {
    $conn_status = "Connection Not Ok";
    // $str = pg_last_error();
}

if(isset($_REQUEST['opera']))
{

    $opera = $_REQUEST['opera'];
    if($opera == 'addservice'){
        add_service();
    }else if($opera == 'addcontent'){
        add_content();
    }else if($opera == 'addpartner'){
        add_partner();
    }else if($opera == 'updatepartner'){
        update_partner();
    }else if($opera == 'deletepartner'){
        delete_partner();
    }else if($opera == 'addcredit'){
        add_credit();
    } elseif ($opera == 'add_candidates') {
        add_candidates();
    }elseif ($opera == 'add_students') {  
        add_students();
    }elseif ($opera == 'addtransport') {  
        add_transport_details();
    }elseif ($opera == 'change_status') {  
        change_status();
    }
}
function change_status(){
    $id = pg_escape_string($_REQUEST['id']);
    global $conn;

        
    $sql = "UPDATE transport_transaction_details SET item_status='Received' where id='$id'";
 
    $result = pg_query($conn,$sql);
    
    if($result){
        echo json_encode(true);
        
    }else{
        echo json_encode(false);
    }

    
}
function add_service(){

        $service_name = pg_escape_string($_REQUEST['servicename']);
        $shortcode = pg_escape_string($_REQUEST['shortcode']);
        $keywords = pg_escape_string($_REQUEST['keywords']);
        $service_category = pg_escape_string($_REQUEST['service_category']);
        $service_description = pg_escape_string($_REQUEST['service_description']);
        $service_type = pg_escape_string($_REQUEST['service_type']);
        $delivery_type = pg_escape_string($_REQUEST['delivery_type']);
//        $sdp_product_id = pg_escape_string($_REQUEST['sdp_product_id']);
        $networks = pg_escape_string($_REQUEST['networks']);
        $subscription_message = pg_escape_string($_REQUEST['subscription_message']);
        $welcome_message = pg_escape_string($_REQUEST['welcome_message']);
        $renewal_message = pg_escape_string($_REQUEST['renewal_message']);
        $unsubscription_message = pg_escape_string($_REQUEST['unsubscription_message']);
        $service_owner = pg_escape_string($_REQUEST['service_owner']);
//        $billing_cycle = pg_escape_string($_REQUEST['billing_cycle']);
        $amount = pg_escape_string($_REQUEST['amount']);
        $delivery_per_day = pg_escape_string($_REQUEST['delivery_per_day']);
//        $sdp_id = pg_escape_string($_REQUEST['sdp_id']);

    global $conn;
    $recorddoesntexist = checkdupesindb ( "subscription_service", "service_name",$service_name, "Record already exists. Choose another name." );

    if($recorddoesntexist){

        $sql = "INSERT INTO subscription_service(service_name,service_shortcode,service_keyword,service_category,service_description,sub_type,service_content_delivery_method,service_network,subscription_msg,welcome_msg,renewal_msg,unsub_msg,service_owner,billing_amt,delivery_per_day) VALUES('$service_name','$shortcode','$keywords',
'$service_category','$service_description','$service_type','$delivery_type','$networks','$subscription_message',
'$welcome_message','$renewal_message','$unsubscription_message','$service_owner','$amount','$delivery_per_day');";
        $result = pg_query( $conn, $sql );
        if($result){
            echo true;
        }else{
            echo false;
        }
    }

}

function change_service(){
        $service_id = $_REQUEST['id'];
        $service_name = pg_escape_string($_REQUEST['servicename']);
        $shortcode = pg_escape_string($_REQUEST['shortcode']);
        $keywords = pg_escape_string($_REQUEST['keywords']);
        $service_category = pg_escape_string($_REQUEST['service_category']);
        $service_description = pg_escape_string($_REQUEST['service_description']);
        $service_type = pg_escape_string($_REQUEST['service_type']);
        $delivery_type = pg_escape_string($_REQUEST['delivery_type']);
//        $sdp_product_id = pg_escape_string($_REQUEST['sdp_product_id']);
        $networks = pg_escape_string($_REQUEST['networks']);
        $subscription_message = pg_escape_string($_REQUEST['subscription_message']);
        $welcome_message = pg_escape_string($_REQUEST['welcome_message']);
        $renewal_message = pg_escape_string($_REQUEST['renewal_message']);
        $unsubscription_message = pg_escape_string($_REQUEST['unsubscription_message']);
        $service_owner = pg_escape_string($_REQUEST['service_owner']);
//        $billing_cycle = pg_escape_string($_REQUEST['billing_cycle']);
        $amount = pg_escape_string($_REQUEST['amount']);
        $delivery_per_day = pg_escape_string($_REQUEST['delivery_per_day']);
//        $sdp_id = pg_escape_string($_REQUEST['sdp_id']);

    global $conn;
    $recorddoesntexist = checkdupesindb ( "subscription_service", "service_name",$service_name, "Record already exists. Choose another name." );

    if(!$recorddoesntexist){

        $sql = "UPDATE subscription_service SET service_name='$service_name',service_shortcode='$shortcode',service_keyword='$keywords',service_category='$service_category',service_description='$service_description',sub_type='$service_type',service_content_delivery_method='$delivery_type',service_network='$networks',subscription_msg='$subscription_message',welcome_msg='$welcome_message',renewal_msg='$renewal_message',unsub_msg='$unsubscription_message',service_owner='$service_owner',billing_amt='$amount',delivery_per_day='$delivery_per_day' WHERE id = $service_id";

/*        $sql = "INSERT INTO subscription_service(service_name,service_shortcode,service_keyword,service_category,service_description,sub_type,service_content_delivery_method,service_network,subscription_msg,welcome_msg,renewal_msg,unsub_msg,service_owner,billing_amt,delivery_per_day) VALUES('$service_name','$shortcode','$keywords','$service_category','$service_description','$service_type','$delivery_type','$networks','$subscription_message',
'$welcome_message','$renewal_message','$unsubscription_message','$service_owner','$amount','$delivery_per_day');";*/

        $result = pg_query( $conn, $sql );
        if($result){
            echo true;
        }else{
            echo false;
        }
    }

}



function add_content(){
    global $fname;
    global $conn;
    $msg_title = pg_escape_string($_REQUEST['sms_title']);
    $sms_service = pg_escape_string($_REQUEST['sms_service']);
    $publication_date = pg_escape_string($_REQUEST['publication_date']);
    $publication_date = date('Y-m-d',strtotime($publication_date));
    //echo $publication_date;
    $sms_content = pg_escape_string($_REQUEST['sms_content']);

    $sql = "INSERT INTO mtechghana2.content_messages(title,cat_id,pubdate,description,userid,lastupdate,is_approved) VALUES('$msg_title','$sms_service','$publication_date','$sms_content','$fname','NOW()',1)";
    $result = pg_query ( $conn, $sql );
    if($result){
        echo true;
    }else{
        echo false;
    }
    //echo $result;
}

function change_content(){
    global $fname;
    global $conn;
    $content_id = $_REQUEST['id'];
    $msg_title = pg_escape_string($_REQUEST['sms_title']);
    $sms_service = pg_escape_string($_REQUEST['sms_service']);
    $publication_date = pg_escape_string($_REQUEST['publication_date']);
    $publication_date = date('Y-m-d',strtotime($publication_date));
    //echo $publication_date;
    $sms_content = pg_escape_string($_REQUEST['sms_content']);
    $sql = "UPDATE mtechghana2.content_messages SET title = '$msg_title', cat_id = '$sms_service', pubdate='$publication_date',description='$sms_content',lastupdate='NOW()' WHERE id = $content_id";
    $result = pg_query ( $conn, $sql );
    if($result){
        echo true;
    }else{
        echo false;
    }
    //echo $result;
}

function add_partner(){
    $company = pg_escape_string($_REQUEST['company_name']);
    //$cost = pg_escape_string($_REQUEST['cost']);
   // $account_type = pg_escape_string($_REQUEST['account_type']);
   // $email = pg_escape_string($_REQUEST['email']);
    //$fname = pg_escape_string($_REQUEST['fname']);
    $fname = pg_escape_string($_REQUEST['fname']);
    $phone1 = pg_escape_string($_REQUEST['phone1']);
    //$phone2 = pg_escape_string($_REQUEST['phone2']);
    $xxname = pg_escape_string($_REQUEST['xxname']);    
    $xxpass = md5($_REQUEST['xxpass']);
    $active = 0;
    if(isset($_REQUEST['active'])){
        $active = 1;
    }else{
        $active = 0;
    }
    //$api = md5(getToken(7));
    //$createdby = $_SESSION['firstname'];



    global $conn;
    //$sql = "INSERT INTO account(org,firstname,lastname,email,phone1,phone2,apikey,accounttype,costpersms,active) VALUES('$company','$fname','$lname','$email','$phone1','$phone2','$api','$account_type',$cost,$active); INSERT INTO letin(smsowner,firstname,lastname,xxname,xxpass,access_level,active) VALUES('$api','$fname','$lname','$xxname','$xxpass','1',$active);";
   
      
    $sql = "INSERT INTO schools(school_name,event_title,phone_numbers) VALUES('$company','$fname','$phone1'); INSERT INTO users(firstname,lastname,login,pw,access_level,active) VALUES('$company','$fname','$xxname','$xxpass','1',$active);";
 
    $result = pg_query($conn,$sql);
    if($result){
        echo json_encode(true);
        
    }else{
        echo json_encode(false);
    }
}



function add_transport_details(){
   
    $kilo     = pg_escape_string($_REQUEST['kilo_weight']);
    $pricetagg = pg_escape_string($_REQUEST['pricetagg']);
    $fsender = pg_escape_string($_REQUEST['fsender']);
    $lsender = pg_escape_string($_REQUEST['lsender']);
    $phone1  = pg_escape_string($_REQUEST['phone1']);
    $email   = pg_escape_string($_REQUEST['email']);
    $desc_item = pg_escape_string($_REQUEST['desc_item']);
    $rfirst = pg_escape_string($_REQUEST['rfirst']);
    $rlast =  pg_escape_string($_REQUEST['rlast']);
    $rphone = pg_escape_string($_REQUEST['rphone']);
    $remail = pg_escape_string($_REQUEST['remail']);
    $source = pg_escape_string($_REQUEST['source']);
    $destination = pg_escape_string($_REQUEST['destination']);

      $pass = ''; 

    $chars = "abcdefghijkmnopqrstuvwxyz023456789"; 

    srand((double)microtime()*1000000); 
    $i = 0; 
    while ($i <= 7) { 
        $num = rand() % 33; 
        $tmp = substr($chars, $num, 1); 
        $pass = $pass . $tmp; 
        $i++; 
    } 
     $getCode = $pass;

    global $conn;

        
    $sql = "INSERT INTO transport_transaction_details(item_kilo,price,sender_firstname,sender_lastname,sender_number,sender_email,item_description,receiver_firstname,receiver_lastname,receiver_number,receiver_email,item_code,source,destination,item_status,date_added) VALUES('$kilo','$pricetagg','$fsender','$lsender','$phone1','$email','$desc_item','$rfirst','$rlast','$rphone','$remail','$getCode','$source','$destination','submitted',NOW());";
 
    $result = pg_query($conn,$sql);
    if($result){
        echo json_encode(true);
        
    }else{
        echo json_encode(false);
    }

}
function add_candidates(){

    $company = pg_escape_string($_REQUEST['candidate_name']);  
    $school_name = pg_escape_string($_REQUEST['school']);
    
    global $conn;
    
    //$sql = "SELECT id FROM schools where school_name ilike '%$school_name%'";
    //echo $sql;
   // $result = pg_query ($conn, $sql);


    $sql2 = "INSERT INTO candidates(candidate_name,school_id) VALUES('$company',$school_name);";
 
    $result2 = pg_query($conn,$sql2);
    

    if($result2){
        echo json_encode(true);
        
    }else{
        echo json_encode(false);
    }
}
  

  function genCode(){
     
     $pass = ''; 

    $chars = "abcdefghijkmnopqrstuvwxyz023456789"; 

    srand((double)microtime()*1000000); 
    $i = 0; 
    while ($i <= 7) { 
        $num = rand() % 33; 
        $tmp = substr($chars, $num, 1); 
        $pass = $pass . $tmp; 
        $i++; 
    } 
    return $pass; 
  }

  function add_students(){

    //$company = pg_escape_string($_REQUEST['candidate_name']);  
    $school_name = pg_escape_string($_REQUEST['school']);
    
    global $conn;
    

     $genCode= genCode();

    //$sql = "SELECT id FROM schools where school_name ilike '%$school_name%'";
    //echo $sql;
   // $result = pg_query ($conn, $sql);


    $sql2 = "INSERT INTO students(students_id,school_id) VALUES('$genCode','$school_name');";
 
    $result2 = pg_query($conn,$sql2);
    

    if($result2){
        echo json_encode(1);
        
    }else{
        echo json_encode(0);
    }
}
  

function update_partner(){
    $company = pg_escape_string($_REQUEST['company_name']);
    $cost = pg_escape_string($_REQUEST['cost']);
    $account_type = pg_escape_string($_REQUEST['account_type']);
    $email = pg_escape_string($_REQUEST['email']);
    $fname = pg_escape_string($_REQUEST['fname']);
    $lname = pg_escape_string($_REQUEST['lname']);
    $phone1 = pg_escape_string($_REQUEST['phone1']);
    $phone2 = pg_escape_string($_REQUEST['phone2']);
/*    $xxname = pg_escape_string($_REQUEST['xxname']);    
    $xxpass = md5($_REQUEST['xxpass']);*/
    $apikey = $_REQUEST['apikey'];
    $active = 0;
    if(isset($_REQUEST['active'])){
        $active = 1;
    }else{
        $active = 0;
    }
    // $api = md5(getToken(7));
    //$createdby = $_SESSION['firstname'];



    global $conn;
    $sql = "UPDATE account SET org = '$company', firstname = '$fname',lastname='$lname',email = '$email',phone1='$phone1',phone2='$phone2',accounttype='$account_type',costpersms=$cost,active=$active WHERE apikey = '$apikey'; UPDATE letin SET firstname='$fname',lastname='$lname',active=$active WHERE smsowner = '$apikey';";

    $result = pg_query($conn,$sql);
    if($result){
        echo json_encode(true);
        
    }else{
        echo json_encode(false);
    }
}


function add_credit(){    
    $amount = pg_escape_string($_REQUEST['amount']);
    $apikey = $_REQUEST['apikey'];
    $user = $_SESSION['username'];

    global $conn;
    $sql = "UPDATE account SET credit = credit + $amount WHERE apikey = '$apikey'; INSERT INTO trans(transowner,transwhen,transamount,transagent) VALUES('$apikey',NOW(),$amount,'$user');";

    $result = pg_query($conn,$sql);
    if($result){
        echo json_encode(true);
        
    }else{
        echo json_encode(false);
    }
}


function delete_partner(){
    $api = $_REQUEST['apikey'];


    global $conn;
    $sql = "DELETE FROM account WHERE apikey = '$api'; DELETE FROM letin WHERE smsowner = '$api';";

    $result = pg_query($conn,$sql);
    if($result){
        echo json_encode(true);
        
    }else{
        echo json_encode(false);
    }
}

function checkdupesindb($table, $columntocheck, $valuetocheck, $messagetoadd) {
    global $message;
    global $conn;
    $sql = "SELECT count(*) as cnt from $table where $columntocheck = '$valuetocheck';";
    //echo $sql;
    $result = pg_query ( $conn, $sql );
    //$row = pg_fetch_all( $result );

    while($r1 = pg_fetch_array($result))
    {
        if ($r1 ['cnt'] == 0) {
            return true;
        } else {
            $message [] = $messagetoadd;
            return false;
        }
    }

}

function crypto_rand_secure($min, $max)
{
    $range = $max - $min;
    if ($range < 1) return $min; // not so random...
    $log = ceil(log($range, 2));
    $bytes = (int) ($log / 8) + 1; // length in bytes
    $bits = (int) $log + 1; // length in bits
    $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
    do {
        $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
        $rnd = $rnd & $filter; // discard irrelevant bits
    } while ($rnd >= $range);
    return $min + $rnd;
}

function getToken($length)
{
    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
    $codeAlphabet.= "0123456789";
    $max = strlen($codeAlphabet) - 1;
    for ($i=0; $i < $length; $i++) {
        $token .= $codeAlphabet[crypto_rand_secure(0, $max)];
    }
    return $token;
}