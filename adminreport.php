
                      <?php

                         include_once('includes/config.php');
					           include('includes/func.php');
					        
                            $page_title = "Admin Report";
                            $section = "Admin"; 
                            include ("header.php");
                            if($_SESSION['access_level'] != "5"){    
                                header('location:index.php');
                            }
                            $row = pg_fetch_array(get_company_names());
					   ?>
                        
                          <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    
                    <?php include_once('includes/breadcrumbs/breadcrumbs.php'); ?>
                 
                    <div class="clearfix"></div>
                    <!-- END DASHBOARD STATS 1-->
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">

                                        <form class="form-inline" role="form" id="reportform">   

                                        <div class="form-group">
                                            <label for="company" >School:</label>
                                            <select name="company" id="company" class="form-control">
                                            <?php 
                                            $val = get_company_names();
                                            while($row = pg_fetch_array($val)) { ?>
                                            <option value="<?php echo $row[1] ?>"><?php echo $row[1]; ?></option>
                                            <?php } ?>                                                
                                                
                                            </select>
                                          </div>                                     
                                          <div class="form-group">
                                            <label for="sdate" >Start Date:</label>
                                            <input type="text"  class="form-control date-picker" data-date-format="dd/mm/yyyy" name="sdate" id="sdate">
                                          </div>
                                          <div class="form-group">
                                            <label for="edate">End Date:</label>
                                            <input type="text" class="form-control date-picker" data-date-format="dd/mm/yyyy" name="edate" id="edate">
                                          </div>                                          
                                          <button type="submit" class="btn btn-primary">Generate</button>
                                        </form>  

                                    </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                <br/>
                                <br/>
<table width="100%"String class="display"String id="view_votes"String cellspacing="0"String>

        <thead>

            <tr>
                 
                <th>student_id</th>
                <th>action</th>
                <th>phone_numbers</th>
                <th>date_recorded </th>
                <th>text_recorded</th>
                
            </tr>

        </thead>

        <tfoot>

            <tr>

               <th>Total</th>

                <th></th>

                <th></th>

                <th></th>
                <th></th>
                <th></th>

            </tr>

        </tfoot>

    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                      
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
      
	                      <?php   
					  
					        include ("footer.php");
					   ?>