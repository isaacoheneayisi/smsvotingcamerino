<?php
$page_title = "tisu report";
$section = "mo reports";

include_once('includes/config.php');
include_once('includes/func.php');
include ("header.php");

?>

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">

        <?php include_once('includes/breadcrumbs/breadcrumbs.php'); ?>

        <div class="clearfix"></div>
        <!-- END DASHBOARD STATS 1-->
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-globe"></i>All <?php echo ucwords($page_title) ?> </div>
                            <div class="tools"> </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover" id="view_tisu">
                                <thead>
                                    <tr>
                                        <th> ID</th>
                                        <th> MSISDN</th>
                                        <th> SHORTCODE</th>
                                        <th> SMS TEXT </th>
                                        <th> NETWORK</th>
                                        <th> DATE CREATED</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                        </div>
                    </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->

<?php

include ("footer.php");
?>