
$(document).ready(function() {
	/*$.ajax({ url: 'includes/func.php',
         data: {action: 'getreport'},
         type: 'post',
         success: function(output) {
         	console.log(output);
                      
                  }
}); */   
//setReport();
} );

$('#reportform').submit(function(e){
	e.preventDefault();
	setReport();
});


function setReport(){
	var sdate = $('#sdate').val();
	var edate = $('#edate').val();


var table =	$('#report_table').DataTable({
	drawCallback: function () {
      var api = this.api();
      $( api.table().column(1).footer() ).html('<span style="color:red;font-weight:700;">' +
        api.column( 1, {page:'current'} ).data().sum() + '</span>'
      );
      $( api.table().column(2).footer() ).html('<span style="color:red;font-weight:700;">' +
        api.column( 2, {page:'current'} ).data().sum() + '</span>'
      );
      $( api.table().column(3).footer() ).html('<span style="color:red;font-weight:700;">' +
        api.column( 3, {page:'current'} ).data().sum() + '</span>'
      );
      $( api.table().column(5).footer() ).html('<span style="color:red;font-weight:700;">GHS ' +
         Math.round(api.column( 5, {page:'current'} ).data().sum()/api.column( 5, {page:'current'} ).data().count() * 10000)/10000 + '</span>'
      );
      $( api.table().column(4).footer() ).html('<span style="color:red;font-weight:700;">' +
        Math.round(api.column( 4, {page:'current'} ).data().sum()/api.column( 4, {page:'current'} ).data().count() * 100)/100 + '%</span>'
      );
    },
    dom: 'Bfrtip',
        buttons: [
       {
           extend: 'print',
           footer: true
       },
       {
           extend: 'pdf',
           footer: true
       },
       {
           extend: 'excel',
           footer: true
       },
       {
           extend: 'csv',
           footer: true
       }
       ],
  destroy: true,
     processing: true,
        serverSide: true,        
    	searching: false,
        "ajax": {
            url: 'includes/getreport.php?',
            data: {"sdate":sdate,"edate":edate},
            type: 'POST'
        },

        "columns": [

            { "data": "smswhen2" },

            { "data": "allsms" },

            { "data": "successsms" },

            { "data": "failsms" },
            { "data": "ratex"},
            { "data": "cost"}

        ]

} );
}