
$(document).ready(function() {
  setReport();
} );

$.fn.dataTable.ext.errMode = 'none'; $('#send_transport-id').on('error.dt', function(e, settings, techNote, message) {
 console.log( 'An error occurred: ', message); 
});

var table = null;

function setReport(){


table = $('#send_transport').DataTable({
    /*drawCallback: function () {
      var api = this.api();
      $( api.table().column(1).footer() ).html('<span style="color:red;font-weight:700;">' +
        api.column( 1, {page:'current'} ).data().sum() + '</span>'
      );
      $( api.table().column(2).footer() ).html('<span style="color:red;font-weight:700;">' +
        api.column( 2, {page:'current'} ).data().sum() + '</span>'
      );
      $( api.table().column(3).footer() ).html('<span style="color:red;font-weight:700;">' +
        api.column( 3, {page:'current'} ).data().sum() + '</span>'
      );
      $( api.table().column(4).footer() ).html('<span style="color:red;font-weight:700;">' +
        Math.round(api.column( 4, {page:'current'} ).data().sum()/api.column( 4, {page:'current'} ).data().count() * 100)/100 + '%</span>'
      );
    },*/
    "columnDefs": [ {
            "targets": -1,
            "data": null            
        } ],
    dom: 'Bfrtip',
        buttons: [
       
           'copy','csv','excel','pdf','print'
       
       
       ],
  destroy: true,
     processing: true,
        serverSide: true,        
        searching: true,
        "ajax": {
            url: 'includes/sent_items_client.php',
            type: 'POST'
        },

        "columns": [      
            { "data": "id" },
            { "data": "item_kilo" },
            { "data": "price"},
            { "data": "item_code"},
            { "data": "sender_lastname"},
            { "data": "receiver_lastname"},
            { "data": "source"},
            { "data": "destination"},
            { "data": "item_status"},
            { "data": "date_added"}
            
        ]


} );
}

/*$('#view_partners tbody').on( 'click', 'button', function () {
        var data = table.row( $(this).parents('tr') ).data();
        console.log(data);
        alert( data['accounttype'] +"  "+ data[ 2 ] );
    } );*/

//var $modal = $('#ajax-modal');
 
$('#send_transport tbody').on( 'click', '.change_partner', function(){
var data = table.row( $(this).parents('tr') ).data();
var vid = data['id'];
  // create the backdrop and wait for next modal to be triggered
  //$('body').modalmanager('loading');
  //alert(vid);

  $.blockUI({ css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        } }); 

 var ajaxRequest =  $.ajax({ 
            url: 'includes/manage_post.php',
            type: 'POST',            
            data: {             
              id:''+vid,
              opera : 'change_status'
          }
            
        });

  ajaxRequest.done(function (xhr, textStatus) {
        $.unblockUI();
        console.log(xhr);
        if(xhr=='true') {
          toastr.success("Item has been well received by the reciver", "Status");
          // $('#view_transport')[0].reset();
           window.location.reload();
        }else {
            toastr.error("The was an error uptaing item Status", "Status");
           
        }

    });

 /* setTimeout(function(){
     $modal.load('updatepartner.php?id='+vid, '', function(){
      $modal.modal();
    });
  }, 1000);*/
});


/*$('#view_transport tbody').on( 'click', '.add_credit', function(){
var data = table.row( $(this).parents('tr') ).data();
var vid = data['id'];
  // create the backdrop and wait for next modal to be triggered
  $('body').modalmanager('loading');
 
  setTimeout(function(){
     $modal.load('updatecredit.php?id='+vid, '', function(){
      $modal.modal();
    });
  }, 1000);
});*/


/*$modal.on('click', 'mm', function(){
  $modal.modal('loading');
  setTimeout(function(){
    $modal
      .modal('loading')
      .find('.modal-body')
        .prepend('<div class="alert alert-info fade in">' +
          'Updated!<button type="button" class="close" data-dismiss="alert">&times;</button>' +
        '</div>');
  }, 1000);
});*/














////////////////////////////////////////////////////////update parteenres//////////////////////////////////////////////////////////////////////////
/*
$modal.on('click', '.deleteaccount', function(){

var api = $('.apikey').val();
bootbox.confirm('<h4 class="text-center">Are you sure you want to delete this account?</h4>',function(result) { 
   // console.log("Confirm result: "+result); 
   if(result){
    $.ajax({ url: 'includes/manage_post.php',
         data: {opera: 'deletepartner', apikey : api},
         type: 'post',
         success: function(output) {
          if(output == 'true'){
            toastr.success("Account has been deleted", "Status");
            window.location.reload();          
                  }
                else{
                  toastr.error("Sorry An Error Occured Please Check and Try Again", "Status");
                }
              }
            
});   
   }
 }); 

  });

$modal.on('submit', '#update_partner', function(e){
e.preventDefault();
console.log($("#update_partner").valid());
if($("#update_partner").valid()){

    $.blockUI({ css: { 
            border: 'none', 
            padding: '15px',
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        } }); 


    var formData = new FormData($('#update_partner')[0]);
    var ajaxRequest = $.ajax({
        type: "POST",
        url: 'includes/manage_post.php',
        contentType: false,
        processData: false,
        data:formData

    });
    ajaxRequest.done(function (xhr, textStatus) {
        $.unblockUI();
        console.log(xhr);
        if(xhr=='true') {
          toastr.success("New Partner Has Been Added Successfully", "Status");
           $('#update_partner')[0].reset();
           window.location.reload();
        }else {
            toastr.error("Sorry An Error Occured Please Check and Try Again", "Status")
           
        }

    });

  }
 });





$modal.on('submit', '#add_credit', function(e){
e.preventDefault();
console.log($("#add_credit").valid());
if($("#add_credit").valid()){

    $.blockUI({ css: { 
            'z-index':9999999,
            border: 'none', 
            padding: '15px',
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        } }); 


    var formData = new FormData($('#add_credit')[0]);
    var ajaxRequest = $.ajax({
        type: "POST",
        url: 'includes/manage_post.php',
        contentType: false,
        processData: false,
        data:formData

    });
    ajaxRequest.done(function (xhr, textStatus) {
        $.unblockUI();
        console.log(xhr);
        if(xhr=='true') {
          toastr.success("Account has been Credited Successfully", "Status");
           $('#add_credit')[0].reset();
           window.location.reload();
        }else {
            alert("Sorry An Error Occured Please Check and Try Again");
           
        }

    });

  }
 });*/