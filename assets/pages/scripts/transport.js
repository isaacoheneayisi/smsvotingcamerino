var add_user_form = $("#add_transport").validate({
	rules: {
		kilo_weight: "required",
		fsender: "required",
		lsender: "required",
		desc_item: "required",
		rfirst: "required",
		rlast: "required",
		rphone: "required",	
		source: "required",
		destination: "required",
		email: {
			required: true,
			email: true
		},
		remail: {
			
			email: true
		}
		
  
	},
	messages: {
    email : "Please enter a valid email",
		kilo_weight: "Please enter the weight of the Item",
		fsender: "Please enter your firstname",
		lsender: "Please enter your surname",
		desc_item: "Please enter description of item",
		phone1: "Enter a valid phone number and a minimum of 10 digits",
		fname:"Please enter the name of the contact person's first name",
		email: "Please enter a valid email address",
		source: "Please choose a valid source",
		destination: "Please choose a valid destination"
		
	}
});

$('#add_transport').submit(function(e){
e.preventDefault();
console.log($("#add_transport").valid());
if($("#add_transport").valid()){

	$.blockUI({ css: { 
            border: 'none', 
            padding: '15px',
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        } }); 


    var formData = new FormData($('#add_transport')[0]);
    var ajaxRequest = $.ajax({
        type: "POST",
        url: 'includes/manage_post.php',
        contentType: false,
        processData: false,
        data:formData

    });
    ajaxRequest.done(function (xhr, textStatus) {
    	$.unblockUI();
        console.log(xhr);
        if(xhr=='true') {
          toastr.success("New Partner Has Been Added Successfully", "Status");
          // $('#add_partner')[0].reset();
           window.location.reload();
        }else {
            toastr.error("Sorry An Error Occured Please Check and Try Again", "Status")
           
        }

    });

  }
 });

$("#kilo").change(function() {
	var test = $("#kilo").val();
   //alert (test);
   if (test === "1-5"){
   	
         $('#lprice').val("5.00");
     	  $('#llprice').val("5");
     	   $('#pricetag').val("5");
     	   $('#pricetagg').val("5");
   } else if (test === "5-10") {
   	 $('#lprice').val("10.00");
   	 $('#llprice').val("10");
   	  $('#pricetag').val("10");
   	  $('#pricetagg').val("10")
   } else if (test === "10-15") {
   	 $('#lprice').val("15.00");
   	 $('#llprice').val("15");
   	  $('#pricetag').val("15");
   	  $('#pricetagg').val("15")
   } else if (test === "15-20") {
   	 $('#lprice').val("20.00");
   	 $('#llprice').val("20");
   	  $('#pricetag').val("20");
   	  $('#pricetagg').val("20")
     }else if (test == "above 20"){
   	 $('#lprice').val("30.00");
   	  $('#llprice').val("30");
   	   $('#pricetag').val("30");
   	   $('#pricetagg').val("30")
   }else {
   	$('#lprice').val("");
   	$('#llprice').val("");
   	 $('#pricetag').val("");
   	 $('#pricetagg').val("")
   }



});




                                 