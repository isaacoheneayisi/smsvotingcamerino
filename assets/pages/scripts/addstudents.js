var add_user_form = $("#add_students").validate({
	rules: {
		company_name: "required",
		cost: "required",
		candidate_name: "required",
		school: "required",
		xxname: {
			required: true,
			minlength: 3,
			remote: {
			        url: "includes/manage_user.php",
			        type: "post",
			        data: {
			        xxname: function() {
			            return $( "#xxname").val();
			        },
			        opera: function(){
			        	return 'checkuser';
			        }
			    }
			}
  
		},
		xxpass: {
			required: true,
			minlength: 5
		},
		confirm_xxpass: {
			required: true,
			minlength: 5,
			equalTo: "#xxpass"
		},
		email: {
			required: true,
			email: true
		},
		phone1:"required",
		phone2 :"required",
		fname:"required"			
	},
	messages: {
		company_name: "Please enter name of company",
		cost: "Please enter cost per sms",
		phone1:"Please enter telephone number for contact person",
		fname:"Please enter the name of the contact person's first name",
		xxname: {
			required: "Please enter a username",
			minlength: "Your username must consist of at least 3 characters"
		},
		xxpass: {
			required: "Please provide a password",
			minlength: "Your password must be at least 5 characters long"
		},
		confirm_xxpass: {
			required: "Please provide a password",
			minlength: "Your password must be at least 5 characters long",
			equalTo: "Please enter the same password as above"
		},
		email: "Please enter a valid email address"
		
	}
});

$('#add_students').submit(function(e){
e.preventDefault();
console.log($("#add_students").valid());
if($("#add_students").valid()){

	$.blockUI({ css: { 
            border: 'none', 
            padding: '15px',
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        } }); 


    var formData = new FormData($('#add_students')[0]);
    var ajaxRequest = $.ajax({
        type: "POST",
        url: 'includes/manage_post.php',
        contentType: false,
        processData: false,
        data:formData

    });
    ajaxRequest.done(function (xhr, textStatus) {
    	$.unblockUI();
        console.log(xhr);
        if(xhr==1) {
          toastr.success("New Student Has Been Added Successfully", "Status");
           $('#add_students')[0].reset();
        }else {
            toastr.error("Sorry An Error Occured Please Check and Try Again", "Status")
           
        }

    });

  }
 });