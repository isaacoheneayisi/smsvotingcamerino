<?php

$page_title = "blast summary";
$section = "reports";

include ("header.php");
?>
                        
        <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <?php include_once('includes/breadcrumbs/breadcrumbs.php'); ?>
            <div class="clearfix"></div>
            <!-- END DASHBOARD STATS 1-->
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-globe"></i><?php echo ucwords($page_title); ?></div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="blast_summary">
                        <thead>
                            <tr>
                                <th>Blast Sequence</th>
                                <th>Date</th>
                                <th>Total Subscribers Serviced</th>
                                <th>Service Name</th>   
                                <th>Shortcode</th> 
                                <th>Messages</th>																								
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php			  
    include ("footer.php");
?>