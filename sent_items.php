
<?php
include_once('includes/config.php');
include_once('includes/func.php');
$page_title = "Sent Items";
$section = "ITEMS"; 
//$success_sms = get_total_success();
//$all_sms = get_total_sms();
//$sms_fail =get_total_fail();
//$success_rate =0;
//if($all_sms > 0)$success_rate = round(($success_sms/$all_sms)* 100); 

include ("header.php"); 



?>
<script type="text/javascript">
    setTimeout(function () { 
      location.reload();
    }, 60 * 1000);
</script>

<style type="text/css">
    
    .img-circle {
    border-radius: 50%;
}

</style>
        <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            
                    <?php include_once('includes/breadcrumbs/breadcrumbs.php'); ?>
                    
           
                    <div class="clearfix"></div>
                    <!-- END DASHBOARD STATS 1-->
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-globe"></i>All Contents </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                     <table class="table table-striped table-bordered table-hover" id="send_transport" width="100%">
                                <thead>
                                    <tr>
                                        <th> ID</th>
                                        <th> Item Kilo</th>
                                        <th> Price</th>
                                        <th> Item Code</th>
                                        <th> Sender Lastname</th> 
                                        <th> Receiver LastName</th>
                                        <th> Source</th>
                                        <th> Destination</th>
                                        <th> Status</th>
                                        <th> Date</th>
                                       
                                        </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                                </div>
                            </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php   
    include ("footer.php");
?>