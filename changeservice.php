
<?php
    include_once('includes/config.php');
    include_once('includes/func.php');
    
    $service_id = $_REQUEST['service_id'];
    $is_post_form=true;
    $page_title = "Home";
	$page_menu ="Services";
    $sub_page_title = "Add Service";
    $section = "services";

    $row = pg_fetch_array(get_service_data($service_id));
    $servicename = $row['service_name'];
    $service_owner = $row['service_owner'];
    $service_shortcode = $row['service_shortcode'];
    $service_keyword = $row['service_keyword'];
    $service_unsubscribe_keyword = $row['service_unsubcribe_keyword'];
    $service_category = $row['service_category'];
    $billing_amt = $row['billing_amt'];
    $service_period = $row['service_period'];
    $subscription_msg = $row['subscription_msg'];
    $unsub_msg = $row['unsub_msg'];
    $service_description = $row['service_description'];
    $sub_type = $row['sub_type'];
    $welcome_msg = $row['welcome_msg'];
    $service_network = $row['service_network'];
    $renewal_msg = $row['renewal_msg'];
    $delivery_per_day = $row['delivery_per_day'];

    $service_content_delivery_method = $row['service_content_delivery_method'];

    $dmethod = array("ByDate"=>"byDate","Trivia"=>"trivia","Voting"=>"voting","Draw"=>"draw");

     $stype= array("MT" => "MT","MO" => "MO");
     $scat= array("MT" => "18","MO" => "19");


    include ("header.php");
?>
                        
                          <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    
                    <?php include_once('includes/breadcrumbs/breadcrumbs.php'); ?>
                 
                    <div class="clearfix"></div>
                    <!-- END DASHBOARD STATS 1-->
                            <!-- Main content page-->
                         <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN VALIDATION STATES-->
                            <div class="portlet light portlet-fit portlet-form bordered">
                                
                                <div class="portlet-body">
                                    <!-- BEGIN FORM-->
                                    <form action="#" id="changeservice" class="form-horizontal">
                                        <input type="hidden" name="opera" id="opera" value="changeservice"/>
                                        <input type="hidden" name="id" value="<?php echo $service_id; ?>">
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">SERVICE NAME
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="servicename" data-required="1" class="form-control" value="<?php echo $servicename; ?>"  required/> </div>
                                            </div>       
                                               <div class="form-group">
                                                <label class="control-label col-md-3">SHORT CODE
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="shortcode" data-required="1" class="form-control" value="<?php echo $service_shortcode; ?>" required/> </div>
                                            </div>   

											  <div class="form-group">
                                                <label class="control-label col-md-3">KEYWORDS(KEYWORDS FOR SMS)
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="keywords" data-required="1" class="form-control" value="<?php echo $service_keyword; ?>" required/> </div>
                                            </div>       
                                           
                                            <div class="form-group">
                                                <label class="control-label col-md-3">SERVICE CATEGORY
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <select class="form-control select2me" name="service_category">
                                                        <option value="">Service Category</option>
                                                        <?php foreach ($scat as $key => $value) { ?>
                                                          <option <?php if($service_category == $value){ ?> selected="selected" <?php } ?> value="<?php echo $value; ?>"><?php echo $key; ?><option>
        
                                                            <?php } ?>                                                       
                                                        
                                                    </select>
                                                </div>
                                            </div>
											
											<div class="form-group">
                                                <label class="control-label col-md-3">SERVICE DESCRIPTION</label>
                                               
                                                    
                                                   <div class="col-md-5">
                                                     <textarea class="form-control" rows="8" name="service_description" required><?php echo $service_description; ?></textarea>                                    
                                                </div>
                                               
                                            </div>
											 <div class="form-group">
                                                <label class="control-label col-md-3">SERVICE TYPE
                                                    <span class="required"> * </span>
                                                </label>
											<div class="col-md-4">
                                                    <select class="form-control select2me" name="service_type">
                                                        <option value="">SERVICE TYPE</option>
                                                        <?php foreach ($stype as $key => $value) { ?>
                                                          <option <?php if($sub_type == $value){ ?> selected="selected" <?php } ?> value="<?php echo $value; ?>"><?php echo $key; ?><option>
        
                                                            <?php } ?>
                                                    </select>
                                                </div>
												</div>
												 <!--<div class="form-group">
                                                <label class="control-label col-md-3">CONNECTION TYPE
                                                    <span class="required"> * </span>
                                                </label>
												  <div class="col-md-4">
                                                    <select class="form-control select2me" name="options2">
                                                        <option value="">CONNECTION TYPE</option>
                                                        <option value="Option 1">SMPP<option>
                                                        <option value="Option 2">SDP</option>
                                                    </select>
                                                </div>
												</div>-->
												 <div class="form-group">
                                                <label class="control-label col-md-3">DELIVERY TYPE
                                                    <span class="required"> * </span>
                                                </label>
												  <div class="col-md-4">
                                                    <select class="form-control select2me" name="delivery_type">
                                                        <option value="">DELIVERY METHOD</option>
                                                        <?php foreach ($dmethod as $key => $value) { ?>
                                                          <option <?php if($service_content_delivery_method == $value){ ?> selected="selected" <?php } ?> value="<?php echo $value; ?>"><?php echo $key; ?><option>
        
                                                            <?php } ?>
                                                    </select>
                                                </div>
												</div>
												 
												<!-- <div class="form-group">
                                                <label class="control-label col-md-3">SDP PRODUCT ID
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="sdp_product_id" data-required="1" class="form-control" /> </div>
                                            </div>  -->
											<div class="form-group">
                                                <label class="control-label col-md-3">NETWORK(S)(Comma Seperated)
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="networks" data-required="1" class="form-control" value="<?php echo $service_network; ?>"  required/> </div>
                                            </div>  
                                             <div class="form-group">
                                                <label class="control-label col-md-3">SUBSCRIPTION MESSAGE</label>
                                               
												
												<div class="col-md-5">
                                                     <textarea class="form-control" rows="6" name="subscription_message" required><?php echo $subscription_msg; ?></textarea>                                    
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="control-label col-md-3">WELCOME MESSAGE</label>
                                                										
												<div class="col-md-5">
                                                     <textarea class="form-control" rows="6" name="welcome_message" required><?php echo $welcome_msg; ?></textarea>                                    
                                                </div>
                                            </div>
											
											<div class="form-group">
                                                <label class="control-label col-md-3">RENEWAL MESSAGE</label>
                                               	<div class="col-md-5">
                                                     <textarea class="form-control" rows="6" name="renewal_message" required><?php echo $renewal_msg; ?></textarea>                                    
                                                </div>
												
                                            </div>
											<div class="form-group">
                                                <label class="control-label col-md-3">UN-SUBSCRIPTION MESSAGE</label>
                                                <div class="col-md-5">
                                                     <textarea class="form-control" rows="6" name="unsubscription_message" required><?php echo $unsub_msg; ?></textarea>                                    
                                                </div>
												
                                            </div>
											 <div class="form-group">
                                                <label class="control-label col-md-3">SERVICE OWNER
                                                    <span class="required"> * </span>
                                                </label>
											  <div class="col-md-4">
                                                    <select class="form-control select2me" name="service_owner">
                                                        <option value="">SELECT OWNER</option>
                                                        
                                                        <?php
                                                        $results = get_service_owner();
                                                        while($r1 = pg_fetch_array($results))
                                                        { ?>
                                                            
                                                            <option <?php if($service_owner == $r1['id']){ ?> selected="selected" <?php } ?> value="<?php echo $r1['id']; ?>" ><?php echo $r1['partner_name']; ?> <option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
												</div>
												<!--  <div class="form-group">
                                                <label class="control-label col-md-3">BILLING CYCLE
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="billing cycle" data-required="1" class="form-control" /> </div>
                                            </div>  -->
											
											 <div class="form-group">
                                                <label class="control-label col-md-3">AMOUNT
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="amount" data-required="1" value="<?php echo $billing_amt; ?>" class="form-control" /> </div>
                                            </div> 
											 <div class="form-group">
                                                <label class="control-label col-md-3">DELIVERY PER DAY
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="delivery_per_day" data-required="1" class="form-control" value="<?php echo $delivery_per_day; ?>" /> </div>
                                            </div> 
											<!-- <div class="form-group">
                                                <label class="control-label col-md-3">SDP SERVICE ID
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="sdp_id" data-required="1" class="form-control" /> </div>
                                            </div>  -->  
                                                                            
                                                                                      
                                           
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn green">Update</button>
                                                    <button type="button" class="btn default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                                <!-- END VALIDATION STATES-->
                            </div>
                        </div>
                    </div>
                </div>
                            <!-- end main content page-->
                      
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
      
	                      <?php   
					  
					        include ("footer.php");
					   ?>