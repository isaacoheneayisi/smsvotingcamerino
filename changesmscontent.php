<?php

    $is_post_form=true;
    $page_title = "Change sms content";
    $section = "contents";
    require_once('includes/config.php');
    require_once('includes/func.php');
    $content_id = $_REQUEST['contentid'];
    include ("header.php");
?>
                        
                          <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">

                    <?php include_once('includes/breadcrumbs/breadcrumbs.php'); ?>

                    <div class="clearfix"></div>
                    <!-- END DASHBOARD STATS 1-->
                            <!-- Main content page-->
                         <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN VALIDATION STATES-->
                            <div class="portlet light portlet-fit portlet-form bordered">
                                <div class="portlet-body">
                                    <!-- BEGIN FORM-->
                                    <form action="" method="post" id="changecontent" class="form-horizontal">
                                        <input type="hidden" name="opera" value="changecontent"/>

                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                                            <?php
                                            $results = pg_fetch_array(get_content($content_id));
                                            $title = $results['title'];
                                            $content = $results['description'];
                                            $service_id = $results['cat_id'];
                                            $id = $results['id'];
                                            $pubdate = date('d-m-Y',strtotime($results['pubdate']));
                                            

                                            ?>
                                                <input type="hidden" name="id" value="<?php echo $id;?>">


                                            <div class="form-group">
                                                <label class="control-label col-md-3">SMS TITLE
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="sms_title" id="sms_title" data-required="1" class="form-control" value="<?php echo $title ?>" required> </div>
                                            </div>                                           
                                           
                                            <div class="form-group">
                                                <label class="control-label col-md-3">SMS SERVICE
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <select class="form-control select2me" id="servicename" name="sms_service" required>
                                                        <option value="ser">Services</option>
                                                        <?php
                                                        $results = get_services();
                                                        while($r1 = pg_fetch_array($results))
                                                        {
                                                            ?>
                                                            <option value="<?php echo $r1['id']; ?>" <?php if($service_id == $r1['id']){ ?> selected="selected" <?php } ?> > <?php echo $r1['service_name']; ?><option>
                                                            <?php
                                                        }
                                                        ?>
                                                        
                                                    </select>
                                                </div>
                                            </div>                                            
											
											
                                            <div class="form-group">
                                                <label class="control-label col-md-3">PUBLICATION DATE</label>
                                                <div class="col-md-4">
                                                    <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                                                        <input type="text" class="form-control" readonly value="<?php echo $pubdate; ?>" name="publication_date" required>
                                                        <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </span>
                                                    </div>
                                                    <!-- /input-group -->
                                                    
                                                </div>
                                            </div>                                 
                                                                                      
                                            <div class="form-group last">
                                                <label class="control-label col-md-3">SMS CONTENT
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-5">
                                                     <textarea class="form-control" rows="5"  name="sms_content" id="sms_content" required><?php echo $content; ?></textarea>                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn green">Update</button>
                                                    <button type="button" class="btn default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                                <!-- END VALIDATION STATES-->
                            </div>
                        </div>
                    </div>
                </div>
                            <!-- end main content page-->
                      
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>

            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
      
	                      <?php   
					  
					        include ("footer.php");
					   ?>