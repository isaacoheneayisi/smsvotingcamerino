
                      <?php   
					  
					        include ("header.php");
					   ?>
                        
                          <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    
                    <?php include_once('includes/breadcrumbs/breadcrumbs.php'); ?>
                 
                    <div class="clearfix"></div>
                    <!-- END DASHBOARD STATS 1-->
                            <!-- Main content page-->
                         <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN VALIDATION STATES-->
                            <div class="portlet light portlet-fit portlet-form bordered">
                                
                                <div class="portlet-body">
                                    <!-- BEGIN FORM-->
									 <div>
                                                <label class="col-md-12" align="center" style="color:red;padding-left:100px">NB: Excel column format: title of SMS | description | publication date(yyyy/mm/dd)
                                                    
                                                </label>
                                              
                                            </div>	
                                    <form action="#" id="form_sample_3" class="form-horizontal">
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                                                                                    
                                           				   
										   
                                            <div class="form-group">
                                                <label class="control-label col-md-3">SMS SERVICE
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <select class="form-control select2me" name="options2">
                                                        <option value="">Services</option>
                                                        <option value="Option 1">News</option>
                                                        <option value="Option 2">sports</option>
                                                        <option value="Option 3">Entertainment</option>
                                                        
                                                    </select>
                                                </div>
                                            </div>
											
											<div class="form-group">
                                                <label class="control-label col-md-3">XLS FILE
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                   <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="input-group input-large">
                                                            
                                                            <span class="input-group-addon btn default btn-file">
                                                                <span class="fileinput-new"> Select file </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="..."> </span>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											
                                                                      
                                                                                      
                                        
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn green">Submit</button>
                                                    <button type="button" class="btn default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                                <!-- END VALIDATION STATES-->
                            </div>
                        </div>
                    </div>
                </div>
                            <!-- end main content page-->
                      
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
      
	                      <?php   
					  
					        include ("footer.php");
					   ?>