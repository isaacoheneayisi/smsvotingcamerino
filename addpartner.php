   <style type="text/css">
       
       .blink_me {
  animation: blinker 2s linear infinite;
}

@keyframes blinker {  
  50% { opacity: 0; }
}
   </style>
                      <?php  
                      $page_title = "Add Transport";
                         $page_menu ="Third Party";
                         $sub_page_title = "Party Details";
                         $section = "Admin";
                         
					     include_once('includes/config.php');
                         include_once('includes/func.php');
                         include ("header.php");
                         /*if($_SESSION['access_level'] != "5"){
                            header('location:index.php');

                        }  		*/			        
					   ?>
                        
                          <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                
                    <?php include_once('includes/breadcrumbs/breadcrumbs.php'); ?>
                 
                    <div class="clearfix"></div>
                    <!-- END DASHBOARD STATS 1-->
                            <!-- Main content page-->
                           <br />
                           <br />

                         <div class="row">
                         <div class="col-md-2" >
                         <div >
                                <h4> GHC  </h4>
                              <input type="text" name="lprice" id="lprice" data-required="1" class="form-control blink_me" disabled="" style="height: 100px; color: red; font-size: 60px" />
                         </div>
                           
                        </div>
                        <div class="col-md-10">
                            <!-- BEGIN VALIDATION STATES-->
                            <div class="portlet light portlet-fit portlet-form bordered ">
                               
                                <div class="portlet-body">
                                    <!-- BEGIN FORM-->
                                    <form id="add_transport" autocomplete="false" class="form-horizontal">
                                    <input type="hidden" name="opera" value="addtransport"/>
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                                           
                                            <div class="form-group">
                                                <label class="control-label col-md-3"> Item Kilo
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <select name="kilo_weight" id="kilo" class="form-control">
                                                <option value=""> Select Kilo<b>(kg)</b></option>                                               
                                               <option value="1-5">1-5 <b>kg</b></option>
                                               <option value="5-10">5-10 <b>kg</b></option>
                                               <option value="10-15">10-15 <b>kg</b></option>
                                               <option value="15-20">15-20<b>kg</b></option>
                                               <option value="above 20">above 20 <b>kg</b></option>
                                            </select>

                                                     </div>
                                            </div>   

                                            
                                                <div class="form-group">
                                                <label class="control-label col-md-3">Price
                                                    <span class="required"> * </span>
                                                    GHC
                                                </label>
                                                <div class="col-md-4">
                                                     <input type="text" name="pricetag" id="pricetag" data-required="1" class="form-control" disabled="" />
                                                      <input type="hidden" name="pricetagg" id="pricetagg" data-required="1" class="form-control"  />
                                                      </div>
                                            </div> 
                                              
                                              <!-- ************** receiver details ***************** -->
                                               <h2> Sender Details </h2>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Sender Firstname 
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="fsender" id="fsender" data-required="1" class="form-control" /> </div>
                                            </div>       
                                               <div class="form-group">
                                                <label class="control-label col-md-3">Sender Lastname
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="lsender" id="lsender" data-required="1" class="form-control" /> </div>
                                            </div>   

                                              
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Sender Number
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="phone1" id="phone1" data-required="1" class="form-control" /> </div>
                                            </div>
                                             
                                             <div class="form-group">
                                                <label class="control-label col-md-3">Sender Email
                                                    <span class="required"> * </span> 
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="email" name="email" id="email"  class="form-control" data-required="1"  class="form-control" /> </div>
                                            </div> 
                                             <div class="form-group">
                                                <label class="control-label col-md-3">Item Description
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <textarea name="desc_item" id="desc_item" data-required="1" class="form-control" rols="5" cols="4"> </textarea> </div>
                                            </div> 

                                             <h2> Receiver Details </h2>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Receiver Firstname 
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="rfirst" id="rfirst" data-required="1" class="form-control" /> </div>
                                            </div>       
                                               <div class="form-group">
                                                <label class="control-label col-md-3">Receiver Lastname
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="rlast" id="rlast" data-required="1" class="form-control" /> </div>
                                            </div>   

                                              
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Receiver Number
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="tel" name="rphone" id="rphone" data-required="1" class="form-control" /> </div>
                                            </div>
											 
											 <div class="form-group">
                                                <label class="control-label col-md-3">Receiver Email
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="email" name="remail" id="remail"  class="form-control" /> </div>
                                            </div> 
                                                                                
                                            <div class="form-group">
                                                <label class="control-label col-md-3"> Source
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <select name="source" id="Isource" class="form-control">
                                            <option value="#">Select Source</option>
                                            <option value="Accra">Accra</option>
                                            <option value="Kumasi">Kumasi</option>
                                                                                       
                                                
                                            </select>

                                                     </div>
                                            </div>   
                                             <div class="form-group">
                                                <label class="control-label col-md-3"> Destination
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <select name="destination" id="Idestination" class="form-control">
                                            <option value="#">Select Destination</option>
                                            <option value="Accra">Accra</option>
                                            <option value="Accra">Kumasi</option>
                                                                                           
                                                
                                            </select>

                                                     </div>
                                            </div>   

                                         
											                           
                                           
                                        <div class="form-actions">

                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn green">Submit</button>
                                                    <button type="button" class="btn default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM
                                </div>
                                <!-- END VALIDATION STATES-->
                            </div>
                        </div>
                    </div>
                </div>
                            <!-- end main content page-->
                      
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
      
	                      <?php   
					  
					        include ("footer.php");
					   ?>