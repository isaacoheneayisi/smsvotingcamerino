
                      <?php   
					       $page_title = "Add User";
					        include ("header.php");

					   ?>
                        
                          <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    
                    <?php include_once('includes/breadcrumbs/breadcrumbs.php'); ?>
                 
                    <div class="clearfix"></div>
                    <!-- END DASHBOARD STATS 1-->
                            <!-- Main content page-->
                         <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN VALIDATION STATES-->
                            <div class="portlet light portlet-fit portlet-form bordered">
                                
                                <div class="portlet-body">
                                    <!-- BEGIN FORM-->
                                    <form id="add_user_form" method="post" class="form-horizontal">
                                    <input type="hidden" name="opera" value="adduser"/>
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>LOGIN </b>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="xxname" id="xxname" data-required="1" class="form-control" /> </div>
                                            </div>       
                                               <div class="form-group">
                                                <label class="control-label col-md-3"><b>PASSWORD </b>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="password" id="xxpass" name="xxpass" data-required="1" data-validation="length" data-validation-length="min8" class="form-control" /> </div>
                                            </div>   

											  <div class="form-group">
                                                <label class="control-label col-md-3"><b>CONFIRM PASSWORD </b>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="password" name="confirm_xxpass" id="confirm_xxpass" data-required="1" class="form-control" /> </div>
                                            </div>   
                                             <div class="form-group">
                                                <label class="control-label col-md-3"><b>FIRSTNAME </b>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="fname" data-required="1" class="form-control" /> </div>
                                            </div>  
											 <div class="form-group">
                                                <label class="control-label col-md-3"><b>LASTNAME </b>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="lname" data-required="1" class="form-control" /> </div>
                                            </div>  
											 <div class="form-group">
                                                <label class="control-label col-md-3"><b>EMAIL</b>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="email" name="email" id="email" data-required="1" class="form-control" /> </div>
                                            </div>  
											 <!-- <div class="form-group">
                                                <label class="control-label col-md-3"><b>PERMISSIONS</b>
                                                    
                                                </label>
                                                <div class="col-md-9">
                                                    <div class="checkbox-list">
                                                        <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox21" value="option1">VIEW</label>
                                                        <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox22" value="option2"> UPDATE </label>
														<label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox22" value="option3"> ADD </label>
															
														<label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox22" value="option4"> DELETE</label>
                                                        
                                                    </div>
                                                </div>
                                            </div>	 -->	
                                            	 <div class="form-group">
                                                <label class="control-label col-md-3"><b>AREA OF PRIVILEGES </b>
                                                    
                                                </label>
                                                <div class="col-md-9">
                                                <select id="user_group" name="user_group">
                                                    <option value="">Select User Group</option>
                                                    <option value="111">SUPPORT(TELCOS)</option>
                                                    <option value="222">SMS CONTENT</option>
                                                    <option value="333">REPORT & STATISTICS</option>
                                                    <option value="444">THIRD PARTY / PARTNER</option>
                                                    <option value="555">ADMIN</option>
                                                </select>
                                                    <!-- <div class="checkbox-list">
                                                        <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox21" value="option1" >SUPPORT(TELCOS)</label>
                                                        <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox22" value="option2"> SMS CONTENT </label>
														<label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox22" value="option2"> REPORT & STATISTICS </label>
														<label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox22" value="option2"> THIRD PARTY / PARTNER </label>
														<label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox22" value="option2"> ADMIN</label>
                                                        
                                                    </div> -->
                                                </div>
                                            </div>		
                                            <div class="form-group hide network">
                                                <label class="control-label col-md-3"><b>OPERATORS</b>
                                                    
                                                </label>
                                                <div class="col-md-9">
                                                    <div class="checkbox-list ">
                                                        <label class="checkbox-inline">
                                                            <input type="checkbox" name="telval[]" value="mtn" >MTN</label>
                                                        <label class="checkbox-inline">
                                                            <input type="checkbox" name="telval[]" value="tigo">TIGO</label>
														<label class="checkbox-inline">
                                                            <input type="checkbox" name="telval[]" value="airtel">AIRTEL</label>
														
                                                    </div>
													<div class="checkbox-list hide network">
                                                        <label class="checkbox-inline">
                                                            <input type="checkbox" name="telval[]" value="vodafone" >VODAFONE</label>
                                                        <label class="checkbox-inline">
                                                            <input type="checkbox" name="telval[]" value="expresso">EXPRESSO</label>
														<label class="checkbox-inline">
                                                            <input type="checkbox" name="telval[]" value="expresso">GLOBACOMM</label>
														
                                                    </div>
													
                                                </div>
                                            </div>												
                                           
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn green">Submit</button>
                                                    <button type="button" class="btn default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                                <!-- END VALIDATION STATES-->
                            </div>
                        </div>
                    </div>
                </div>
                            <!-- end main content page-->
                      
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
      
	                      <?php   
					  
					        include ("footer.php");
					   ?>