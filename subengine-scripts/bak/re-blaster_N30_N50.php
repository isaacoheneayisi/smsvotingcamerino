<?php

error_reporting(0);
require_once("blaster-config.php");
require_once("blaster-functions.php");
require_once("LiveRollingCurlBlaster.php");

#START-UP PROCEDURE
dir_creator(LOG_LOCATION);
dir_creator(URL_LOG_LOCATION_REBLASTER);
dir_creator(URL_LOG_BACKUP);
dir_creator(DLR_SENT_LOCATION);

define(BLASTER_TYPE, REBLASTER);
define(LOCATION, URL_LOG_LOCATION_REBLASTER);
define(LOG_FILE_NAME, log_file_name(date("Ymd"), BLASTER_TYPE) . ".log");

#CONNECT TO DATABASE
$connection = connect_to_db(CONN_LINUX);

#GET BLAST_SEQ IF IT DOESNT EXIST
$blast_seq = 0;
#$blast_seq = get_blast_seq();
define(BLAST_SEQ, $blast_seq);


$temp_table = "blast_details_" . show_now_file_name();
debug("Blast Table name is " . $temp_table);

$now = date("Y-m-d");

$sql_blast = "
    
DROP TABLE IF EXISTS __dlr_reblast__;

select d.from, d.to, b.message, d.service_id, d.content_id, d.blaster_id, d.blast_seq, d.sub_source
into __dlr_reblast__
from dlr d, blast_summary b
where
b.seq = d.blast_seq and
b.date::date = '{$now}' and
d.date::date = '{$now}' and
d.type = 16 and
b.shortcode  in ('33963','32058') and 
d.content_id = b.content_id and
d.service_id = b.service_id
;


--CREATE TABLE with serial id

CREATE TABLE {$temp_table}
(
  blast_id serial NOT NULL,
  
  --content
  content_id integer NOT NULL,
  valid_date date,
  message text NOT NULL,
  service_id integer NOT NULL,
  
  --services
  service_period integer,
  shortcode character varying(15),
  max_content integer,
  
  --subscription details
  msisdn character varying,
  sub_source character varying,
  
  blaster_id integer
 
  --creation_time timestamp without time zone DEFAULT now(),
)
WITH (
  OIDS=FALSE
);



--SELECT DATA & INSERT DATA INTO TABLE
INSERT INTO {$temp_table}(content_id, valid_date, message, service_id, service_period, 
            shortcode, max_content, msisdn, sub_source, blaster_id)
(
select
    d.content_id,
    now()::date as valid_date,
    d.message,
    d.service_id,
    0 as service_period,
    d.from as shortcode,
    0 as max_content,
    d.to as msisdn,
    d.sub_source,
    d.blaster_id
from
    __dlr_reblast__ d
    
order by 
    d.to
    
    
    
);	

/*
--INSERT SUMMARIZED DATA INTO BLAST SUMMARY
INSERT INTO blast_summary(seq, subs, content_id, service_id, service, shortcode, message)
                                        (
                                                select
                                                        currval('blast_seq'),
                                                        count(msisdn),
                                                        a.content_id,
                                                        a.service_id,
                                                        sx.name as service,
                                                        a.shortcode,
                                                        a.message
                                                from
                                                        {$temp_table} a,
                                                        services sx
                                                where
                                                sx.id = a.service_id                                                

                                                group by
                                                        a.content_id,
                                                        a.service_id,
                                                        sx.name,
                                                        a.shortcode,
                                                        a.message
                                                order by
                                                a.service_id, b.msisdn
                                        );
*/

--SELECT DATA BY ORDER AND SHOW 


                                                select
                                                    b.blast_id,
                                                    b.content_id,
                                                    b.valid_date,
                                                    b.message,
                                                    b.service_id,
                                                    b.shortcode,
                                                    b.msisdn,
                                                    b.sub_source,
                                                    b.blaster_id

                                                from
                                                        {$temp_table} b
                                                order by 
                                                    b.blast_id
                                                ;
   ";



debug($sql_blast);
$time_start = microtime(true);

$result = pg_query($connection, $sql_blast);

$time_end = microtime(true);
$time = $time_end - $time_start;
$testresult1 = "Query Completed in {$time} seconds \n";
sleep(5);
debug($testresult1);
debug(pg_last_error());




#START PROGRESS BAR CACHE
$redis = new Redis();
$redis->connect(REDIS_HOST, REDIS_PORT);
$GLOBALS['REDIS_NAME'] = "progress_bar_" . date("Ymd") . "_" . $tablename;
echo "\n\n REDIS NAME = " . $GLOBALS['REDIS_NAME'] . ".\n\n";





$time_start = microtime(true);
$GLOBALS['time_start'] = $time_start;
$counter = 0;
$total_count = pg_num_rows($result);
#############################################
#############################################
$rc = new RollingCurl("request_callback");
$rc->window_size = CONNECTIONS;
$rc->execute();
#############################################
#############################################
$time_end = microtime(true);
$time = $time_end - $time_start;

$testresult2 = "Completed {$total_count} calls in {$time} seconds OR ".(round($time/60))." mins ( @ ".(get_tps($time,$total_count))." msgs/s with Connections:  ".CONNECTIONS.")\n";
debug($testresult2);


#CLOSE PROGRESS BAR CACHE
$redis->close();


if ($total_count > 100) {
    $sql_delete_tables = "DROP TABLE {$temp_table} ;";
    $delete_result = pg_query($connection, $sql_delete_tables);
    debug("DROPING TABLE: {$temp_table} !");
}

debug("Removing Log File!");
var_dump(move_files(LOG_LOCATION.LOG_FILE_NAME, URL_LOG_BACKUP));
debug("Completely finished!");
unlink(URL_LOG_BACKUP.LOG_FILE_NAME);
?>
