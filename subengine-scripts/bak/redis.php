<?php


  #Things to log
  #blaster-type
  #
  $redis = new Redis();

  $redis->connect('subenginecache.zssf8u.0001.use1.cache.amazonaws.com', 6379);
  //$redis->connect('127.0.0.1'); // port 6379 by default
  //$redis->connect('127.0.0.1', 6379, 2.5); // 2.5 sec timeout.
  //$redis->connect('/tmp/redis.sock'); // unix domain socket.
  //$redis->connect('127.0.0.1', 6379, 1, NULL, 100); // 1 sec timeout, 100ms delay between reconnection attempts.

  $redisname = "progress_bar_".date("Ymd")."_"."instant";
  $redis->set($redisname, date("Y-m-d H:i:s"));
  $timenow = time(NULL); // current timestamp
  $redis->expireAt($redisname, $timenow + (24*60*60)); #will expire in 24 hours
  echo $redis->get($redisname);
  $redis->close();

#$old = NULL;
while (1) {
    $new = roundUpToAny(date("s"), 10);
    cache_progress_bar_caller($new);
}

function roundUpToAny($n, $x = 10) {
    return round(($n + $x / 2) / $x) * $x;
}

function cache_progress_bar_caller($new) {
    global $old;

    echo "new = $new, old = $old \n";

    if ($new != $old) {
        $old = $new;
        echo "==Alert old has just been set to $old == \n";
        sleep(2);
    } else {
        echo "they are the same \n";
        sleep(2);
    }
}

#echo '52 rounded to the nearest 5 is ' . roundUpToAny(52,5) . '<br />';
?>