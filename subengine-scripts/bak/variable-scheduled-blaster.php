<?php
require_once("blaster-config.php");
require_once("blaster-functions.php");
require_once("LiveRollingCurlBlaster.php");

#START-UP PROCEDURE
dir_creator(LOG_LOCATION);
dir_creator(URL_LOG_LOCATION_VARIABLE_SCHEDULED);
dir_creator(URL_LOG_BACKUP);
dir_creator(DLR_SENT_LOCATION);

define(BLASTER_TYPE,BLASTER_VARIABLE_SCHEDULED);
define(LOCATION,URL_LOG_LOCATION_VARIABLE_SCHEDULED);
define(LOG_FILE_NAME,log_file_name(date("Ymd"), BLASTER_TYPE).".log");

#CONNECT TO DATABASE
$connection = connect_to_db(CONN_WINDOWS);

#require_once("varible-instant-detect.php");
#exit;
#GET BLAST_SEQ IF IT DOESNT EXIST
#$blast_seq = 7235;
$blast_seq = get_blast_seq();
define(BLAST_SEQ,$blast_seq);

$tablename = str_replace('-', '_', $GLOBALS['BLASTER_TYPES'][BLASTER_TYPE]);
$temp_table = "blast_details_{$tablename}_".BLAST_SEQ;
debug("Blast Table name is ".$temp_table);

$sql_deactivate = "
    --DEACTIVATE
    --(3 or 4 => 0)
    
    select
    --(NOW()::date - (CASE WHEN last_blast_time is NULL THEN last_subscribed_time ELSE last_blast_time END)::date) as predicted_last_bill_time,*

    a.id into temp __deactivateids__

    from subscription_details a, services b
    where
    b.id = a.service_id and
    a.status = b.status and
    b.status = 4 and
    (NOW()::date - (CASE WHEN last_blast_time is NULL THEN last_subscribed_time ELSE last_blast_time END)::date) > billing_period;
    
    update subscription_details
    set status = 0
    where id in (select * from __deactivateids__) and status = 4;

";




$sql_send_bill = "
    --SEND AND BILL
    --Collate data but do not send yet. Send with Free batch.
    
 select
	0 as content_id,
	now()::date as valid_date,
	b.reminder_message as message, 
	a.service_id, 
	b.service_period,
	b.billing_shortcode, 
	b.max_content,
	a.msisdn, 
	a.sub_source
        
from 
	subscription_details a, services b
where
        a.service_id = b.id and
        b.status = 3 and a.status = 0 and --a.service_id = 361 and
        a.service_id not in (select service_id from blast_summary where date::date = now()::date and content_id = 0)

";

#ACTIVATE
#PERSIST FILE TO DATABASE
$sql_activate = "
    --ACTIVATE
    --(0 => 3 or 4)
    select
        a.id into temp __activateids__
    from 
    ;
    
    update subscription_details
    set status = 4
    where id in (select * from __activateids__) and status = 0;

";

$sql_send_free = "
    --SEND FREE + BILLED
    --CREATE TABLE with serial id

CREATE TABLE {$temp_table}
(
  blast_id serial NOT NULL,
  
  --content
  content_id integer NOT NULL,
  valid_date date,
  message text NOT NULL,
  service_id integer NOT NULL,
  
  --services
  service_period integer,
  shortcode character varying(15),
  max_content integer,
  
  --subscription details
  msisdn character varying,
  sub_source character varying
  
  --creation_time timestamp without time zone DEFAULT now(),
)
WITH (
  OIDS=FALSE
);



--SELECT DATA & INSERT DATA INTO TABLE
INSERT INTO {$temp_table}(content_id, valid_date, message, service_id, service_period, 
            shortcode, max_content, msisdn, sub_source)
(
    (
    select
        c.id as content_id,
        c.valid_date,
        c.message,
        c.service_id,
        ss.service_period,
        ss.shortcode,
        ss.max_content,
        s.msisdn,
        s.sub_source
    from
        subscription_details s,
        content c,
        services ss
    where
        s.service_id = c.service_id         --join subscribers service id with services id
        and s.status = 3                    --only active subscribers
        and ss.status = 3                   --only active services
        and ss.id = c.service_id            --join content service id with services id

        --and s.service_id = 353

        and c.id not in (select content_id from blast_summary where date::date = now()::date and content_id != 0)
        and c.valid_date = now()::date and  --if date is the same
        
        --calculation
        (NOW()::date - s.last_bill_time::date) <= ss.billing_period --days users has been subscribed for
  
     )
 UNION ALL
    (
        {$sql_send_bill}
    )
    
    
);	

--INSERT SUMMARIZED DATA INTO BLAST SUMMARY
INSERT INTO blast_summary(seq, subs, content_id, service_id, service, shortcode, message)
                                        (
                                                select
                                                        currval('blast_seq'),
                                                        count(msisdn),
                                                        a.content_id,
                                                        a.service_id,
                                                        sx.name as service,
                                                        a.shortcode,
                                                        a.message
                                                from
                                                        {$temp_table} a,
                                                        services sx
                                                where
                                                sx.id = a.service_id                                                

                                                group by
                                                        a.content_id,
                                                        a.service_id,
                                                        sx.name,
                                                        a.shortcode,
                                                        a.message
                                                order by
                                                a.service_id
                                        );

--SELECT DATA BY ORDER AND SHOW 


                                                select
                                                    b.blast_id,
                                                    b.content_id,
                                                    b.valid_date,
                                                    b.message,
                                                    b.service_id,
                                                    b.shortcode,
                                                    b.msisdn,
                                                    b.sub_source

                                                from
                                                        {$temp_table} b
                                                order by 
                                                    b.blast_id, b.msisdn
                                                ;

";
#$sql_send_free = "select * from _newtired_ limit 200;";
#$sql_send_free = "select * from {$temp_table} limit 5;";
#$sql_blast = "select * from blast_details_7235 where blast_id > 386791 order by blast_id";


debug($sql_send_free);
$time_start = microtime(true);

$result = pg_query($connection, $sql_send_free);
#exit;
$time_end = microtime(true);
$time = $time_end - $time_start;
$testresult1 = "Query Completed in {$time} seconds \n";
sleep(5);
debug($testresult1);
debug(pg_last_error());




#START PROGRESS BAR CACHE
@$redis = new Redis();
@$redis->connect(REDIS_HOST, REDIS_PORT);
$GLOBALS['REDIS_NAME'] = "progress_bar_" . date("Ymd") . "_" . $tablename;
echo "\n\n REDIS NAME = " . $GLOBALS['REDIS_NAME'] . ".\n\n";



$time_start = microtime(true);
$GLOBALS['time_start'] = $time_start;
$counter = 0;
$total_count = pg_num_rows($result);
#############################################
#############################################
$rc = new RollingCurl("request_callback");
$rc->window_size = CONNECTIONS; 
$rc->execute();
#############################################
#############################################
$time_end = microtime(true);
$time = $time_end - $time_start;

$testresult2 = "Completed {$total_count} calls in {$time} seconds OR ".(round($time/60))." mins ( @ ".(get_tps($time,$total_count))." msgs/s with Connections:  ".CONNECTIONS.")\n";
debug($testresult2);

#CLOSE PROGRESS BAR CACHE
$redis->close();
 
debug("Removing Log File!");
var_dump(move_files(LOG_LOCATION.LOG_FILE_NAME, URL_LOG_BACKUP));
debug("Completely finished!");
unlink(URL_LOG_BACKUP.LOG_FILE_NAME);
?>
