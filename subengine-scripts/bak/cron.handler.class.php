<?php

        class cronHelper {

                private static $pid;

                function __construct() {}

                function __clone() {}

                private static function isrunning() {
                        $pids = explode(PHP_EOL, `ps -e | awk '{print $1}'`);
                        if(in_array(self::$pid, $pids))
                                return TRUE;
                        return FALSE;
                }

                public static function lock() {
                        global $argv;

                        $lock_file = LOCK_DIR.basename($argv[1]).LOCK_SUFFIX;

                        if(file_exists($lock_file)) {
                                //return FALSE;

                                // Is running?
                                self::$pid = file_get_contents($lock_file);
                                if(self::isrunning()) {
                                        echo "==".self::$pid."== Already in progress...\n";
                                        error_log("==".self::$pid."== Already in progress...\n");
                                        return FALSE;
                                }
                                else {
                                        echo "==".self::$pid."== Previous job died abruptly...\n";
                                        error_log("==".self::$pid."== Previous job died abruptly...\n");
                                }
                        }

                        self::$pid = getmypid();
                        file_put_contents($lock_file, self::$pid);
                        echo "==".self::$pid."== Lock acquired, processing the job...\n";
                        error_log("==".self::$pid."== Lock acquired, processing the job...\n");

                        return self::$pid;
                }

                public static function unlock() {
                        global $argv;

                        $lock_file = LOCK_DIR.basename($argv[1]).LOCK_SUFFIX;

                        if(file_exists($lock_file))
                                unlink($lock_file);
                        echo "==".self::$pid."== Releasing lock...\n";
                        error_log("==".self::$pid."== Releasing lock...\n");
                        return TRUE;
                }

        }

?>