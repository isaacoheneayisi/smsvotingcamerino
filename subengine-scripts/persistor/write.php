<?php

/*
 * This will write the url parameters to DB or to file in a format ready to be persisted to DB
 *
 * escaped
 * CSV Format
 */
require_once 'universal.php';

class xdlr_writer extends universalclass {

    protected $from; #%p
    protected $to; #%P
    protected $msg; #%a
    public $content_date;
    public $service_id;
    public $blaster_id;
    public $blast_seq;
    public $smsc_id;
    public $smsc;
    public $type;
    public $content_id;
    public $msg_id;
    public $http_status;

    #DLR DATA
    public $dlr_submit;
    public $dlr_done;
    public $dlr_status;
    public $dlr_smsc; #%i
    public $dlr_duration;
    public $debug;
    public $live;   //persist straight to DB or log to file
    public $writetype; //write to file using linux or php

    const service_id = '0';
    const blaster_id = '0';
    const blast_seq = '0';
    const smsc_id = '0';
    const content_id = '0';
    const type = '0';
    const live = FALSE;
    const writetype = 0; //0 = php (default), 1 = linux
    const dlr_submit = null;
    const dlr_done = null;
    const dlr_status = null;

    public $sub_source = null;

    /*
     * BLASTER ID: USED TO KNOW WHO IS BLASTING. WILL MAINTAIN BLAST SEQ FOR EACH CALL
     * MESSAGE ID: USED TO DIFFERENTIATE MESSAGES AND GROUP THEM (TIME NUMBER CAN BE USED)
     * BLAST SEQUENCE: USED TO DIFFERENTIATE BLASTS FROM EACH OTHER
     * SERVICE ID: USED TO KNOW THE SERVICE. ARBITRARY BLASTS WILL BE 0 AND BLAST SEQUENCE WILL BE USED TO DETERMINE WHICH IS WHICH
     *
     * URL WILL SPECIFY 'NEW'/'TRUE'/'1' AS A PARAMETER TO GENERATE A NEW BLAST SEQ.
     * 
     * CONTENT DATE: DATE CONTENT WAS VALID FOR
     *
     */

    public function xdlr_writer() {
        $this->set_all_options();
        $this->clean_n_format();
        if ($this->debug == true) {
            echo "========DEBUG========<br>\n";
            $this->debug();
            echo "========DEBUG========<br>\n";
        } else {
            if ($this->live !== true) {
                #echo "FILE";

                $this->write_to_file();
            } else {
                #echo "DB";

                $this->write_to_db();
            }
        }

        #exit("hh");
    }

    private function debug() {
        $class_vars = get_object_vars($this);
        var_dump($class_vars);
        foreach ($class_vars as $name => $value) {

            echo "$$name = $value<br>";
            #$$name = $value;
        }
    }

    private function clean_n_format() {
        $class_vars = get_object_vars($this);

        if ($this->live !== true) {
            #echo "false"; FILE
            foreach ($class_vars as $name => $value) {
                #LOOK AT http://www.w3schools.com/tags/ref_urlencode.asp
                #REMOVE CHARS %00 - %1F except for tab and line feed
                $value = str_replace(array("\t", "\n", "\r"), array('\t', '\n', '\r'), $value);
                $value = utf8_encode($value);
                $this->$name = $value;
            }
        } else {
            #echo "true"; DB
            foreach ($class_vars as $name => $value) {
                $value = utf8_encode($value);
                $this->$name = pg_escape_string($value);
            }
        }
        $mno_temp = $this->get_mno($this->to);
        $this->smsc_id = $mno_temp['id'];
        $this->smsc = $mno_temp['name'];
        #var_dump($this->smsc_id);
        #$this->msg = substr($this->msg,0,10);

        if ($this->type == 1 || $this->type == 2) {
            $dlrdata = $this->get_dlr_data($this->msg);
            $this->dlr_submit = $dlrdata[0];
            $this->dlr_done = $dlrdata[1];
            $this->dlr_status = $dlrdata[2];
            $this->dlr_duration = $dlrdata[3];
        } else if ($this->type == 8 || $this->type == 16) {
            $this->dlr_submit = NULL;
            $this->dlr_done = NULL;
            $this->dlr_status = NULL;
            $this->dlr_duration = NULL;
        } else {
            $this->dlr_submit = NULL;
            $this->dlr_done = NULL;
            $this->dlr_status = NULL;
            $this->dlr_duration = NULL;
        }
    }

    private function write_to_db() {
        $class_vars = get_object_vars($this);

        #MAKE VARIABLES OF ATTRIBUTES
        foreach ($class_vars as $name => $value) {

            #echo "$$name = $value<br>";
            $$name = $value;
        }
        $data_to_write = "INSERT INTO dlr(\"from\",\"to\",\"msg\") VALUES (E'" . $from . "',E'" . $to . "',E'" . $msg . "');\n";
        #echo $data_to_write;
        $connect = @pg_connect("host=localhosta port=5432 dbname=test user=postgres password=spikelee");
        $query = @pg_query($data_to_write);
        if ($query && $connect) {
            echo "written to database!\n";
        } else {
            echo "db write failed. \n " . @pg_last_error() . " \nattempting to write to file.\n";
            $write_to_file = $this->insert_string_to_file($data_to_write, $this->sql);
            if ($write_to_file) {
                echo "file written successfully.\n";
            } else {
                echo "write to file failed.\n";
            }
        }
    }

    private function get_dlr_data($dlrmsg) {
        #$regex = '/submit date:(?<submitted>\d+) done date:(?<done>\d+) stat:(?<status_value>\w+)/';
        $regex = '/submit date:(\d+) done date:(\d+) stat:(\w+)/';
        @preg_match($regex, $dlrmsg, $matches);

        $a = @date_parse_from_format("ymdHi", $matches[1]);
        $b = @date_parse_from_format("ymdHi", $matches[2]);
        $c = $matches[3];


        $dlr_submit = @date("Y-m-d H:i:s", @mktime($a['hour'], $a['minute'], $a['second'], $a['month'], $a['day'], $a['year']));
        $dlr_done = @date("Y-m-d H:i:s", @mktime($b['hour'], $b['minute'], $b['second'], $b['month'], $b['day'], $b['year']));

        $dlr_status = $c;
        $dlr_duration = abs(strtotime($dlr_done)) - abs(strtotime($dlr_submit));

        if ($a['year'] < "2010") {
            $dlr_submit = NULL;
            $dlr_duration = NULL;
        }
        if ($b['year'] < "2010") {
            $dlr_done = NULL;
            $dlr_duration = NULL;
        }

        $dlr = array(0 => $dlr_submit, 1 => $dlr_done, 2 => $dlr_status, 3 => $dlr_duration);
        return $dlr;
    }

    private function insert_string_to_file($string, $location) {
        $target_file = @fopen($location, 'a');
        $write_to_file = @fwrite($target_file, $string);
        fclose($target_file);
        if ($target_file && $write_to_file) {
            return true;
        } else {
            return false;
        }
    }

    private function write_to_file() {
        $class_vars = get_object_vars($this);

        #MAKE VARIABLES OF ATTRIBUTES
        foreach ($class_vars as $name => $value) {

            #echo "$$name = $value<br>";
            $$name = $value;
        }


        if ($content_date) {
            $now = $content_date;
        } else {
            $now = date('Y-m-d H:i:s');
        }
        
        $dlr_submit = date('Y-m-d H:i:s');
        
        #CSV VERSION
        #$data_to_write = "'{$from}','$to','$msg'";
        #COPY CSV VERSION
        $data_to_write = $now . "\t" . $from . "\t" . $to . "\t" . substr($msg, 0, 16) . "\t" . $service_id . "\t" . $smsc_id . "\t" . $type . "\t";     #7
        $data_to_write .= $content_id . "\t" . $msg_id . "\t" . $http_status . "\t" . $blaster_id . "\t" . $blast_seq . "\t";                  #5
        $data_to_write .= $dlr_smsc . "\t" . $dlr_submit . "\t" . $dlr_done . "\t" . $dlr_status . "\t" . $dlr_duration . "\t" . $sub_source . "\n";                #6  #18
        #$filename = 'test.txt';
        #$filename = 'csv\dlr1.csv';
        $filename = $this->file;
        #$now = date('Y-m-d H:i:s');
        #$data_to_write = "{$now} - $data_to_write \n";
        #WRITE USING PHP OR LINUX
        if ($this->writetype == 1) {
            #echo "writing with Linux";
            $write_using_php = FALSE;
        } else {
            #echo "writing with PHP";
            $write_using_php = TRUE;
        }


        #WRITE DATA TO FILE
        if ($write_using_php) {
            #PHP
            if (!$handle = fopen($filename, 'a')) {
                echo "Cannot open file ($filename)";
                exit;
            }

            if (fwrite($handle, $data_to_write) === FALSE) {
                echo "Cannot write to file ($filename)";
                exit;
            }

            fclose($handle);
        } else {
            #LINUX
            #`echo $(date) Linux >> test.txt`;
            `DATE /T >> test.txt`;
        }
    }

}

#header("Content-Type: text/plain");
date_default_timezone_set('Africa/Accra');
$grab = new xdlr_writer();
?>
