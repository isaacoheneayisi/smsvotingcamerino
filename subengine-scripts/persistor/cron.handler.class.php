<?php
        define(MAIN_PATH, "/usr/local/apache/httpdocs/subengine-ghana" );
        define('LOCK_DIR', MAIN_PATH . '/subengine-scripts/persistor/');
        #define('LOCK_DIR', '');
        define('LOCK_SUFFIX', '.lock');

        class cronHelper {

                private static $pid;

                function __construct() {}

                function __clone() {}

                private static function isrunning() {
                        $pids = explode(PHP_EOL, `ps -e | awk '{print $1}'`);
                        if(in_array(self::$pid, $pids))
                                return TRUE;
                        return FALSE;
                }

                public static function lock() {
                        global $argv;

                        $lock_file = LOCK_DIR.basename($argv[0]).LOCK_SUFFIX;

                        if(file_exists($lock_file)) {
                                //return FALSE;

                                // Is running?
                                self::$pid = file_get_contents($lock_file);
                                if(self::isrunning()) {
                                        echo date("Y-m-d H:i:s").": ==".self::$pid."== Already in progress...\n";
                                        error_log(date("Y-m-d H:i:s").": ==".self::$pid."== Already in progress...\n");
                                        return FALSE;
                                }
                                else {
                                        echo date("Y-m-d H:i:s").": ==".self::$pid."== Previous job died abruptly...\n";
                                        error_log(date("Y-m-d H:i:s").": ==".self::$pid."== Previous job died abruptly...\n");
                                }
                        }

                        self::$pid = getmypid();
                        file_put_contents($lock_file, self::$pid);
                        echo date("Y-m-d H:i:s").": ==".self::$pid."== Lock acquired, processing the job...\n";
                        error_log(date("Y-m-d H:i:s").": ==".self::$pid."== Lock acquired, processing the job...\n");

                        return self::$pid;
                }

                public static function unlock() {
                        global $argv;

                        $lock_file = LOCK_DIR.basename($argv[0]).LOCK_SUFFIX;

                        if(file_exists($lock_file))
                                unlink($lock_file);
                        echo date("Y-m-d H:i:s").": ==".self::$pid."== Releasing lock...\n";
                        error_log(date("Y-m-d H:i:s").": ==".self::$pid."== Releasing lock...\n");
                        return TRUE;
                }

        }

?>

