<?php

# CONNECTION INFO

#define(CONN_WINDOWS,"host=10.244.62.193 port=5432 dbname=subengine user=mtech password=mtech");
#define(CONN_LINUX,"host=localhost port=5432 dbname=subengine user=postgres password=");

define(CONN_GHANA,"host=localhost port=5432 dbname=subengine-ghana user=postgres password=''");
#define(CONN_LINUX,"host=localhost port=5432 dbname=subengine user=postgres password=");

define(CONNECTIONS,15);
define(DLR_MASK,24);

define(MAIN_PATH, "/usr/local/apache/httpdocs/subengine-ghana" );

define(LOG_LOCATION, MAIN_PATH . "/subengine-scripts/logs/");
define(BILL_LOG_LOCATION, MAIN_PATH . "/subengine-scripts/logs/billing/");
define(URL_LOG_LOCATION_REBLASTER, MAIN_PATH . "/subengine-scripts/logs/reblaster/");
define(URL_LOG_LOCATION_SCHEDULED, MAIN_PATH . "/subengine-scripts/logs/scheduled/");
define(URL_LOG_LOCATION_INSTANT, MAIN_PATH . "/subengine-scripts/logs/instant/");
define(URL_LOG_LOCATION_VARIABLE_SCHEDULED, MAIN_PATH . "/subengine-scripts/logs/variable-scheduled/");
define(URL_LOG_LOCATION_VARIABLE_INSTANT, MAIN_PATH . "/subengine-scripts/logs/variable-instant/");
define(URL_LOG_BACKUP, MAIN_PATH . "/subengine-scripts/logs/done/");
define(DLR_SENT_LOCATION, MAIN_PATH . "/subengine-scripts/logs/dlrs/");
define(DLR_SENT_FILE,DLR_SENT_LOCATION."dlr_sent.csv");
define(TEMPDIR, MAIN_PATH . "/subengine-scripts/temp/");

define('LOCK_DIR', MAIN_PATH . '/subengine-scripts/blasters/');
define('LOCK_SUFFIX', '.lock');
define('CRON_HANDLER_CLASS', 'cron.handler.class.php');

define(BLASTER_SCHEDULED, 1);
define(BLASTER_INSTANT, 2);
define(BLASTER_VARIABLE_SCHEDULED, 3);
define(BLASTER_VARIABLE_INSTANT, 4);
define(REBLASTER, 10);

$GLOBALS['BLASTER_TYPES'] = array( 1=>"scheduled", 2=>"instant", 3=>"variable-scheduled", 4=>"variable-instant", 10=>"reblaster");

#define(REDIS_HOST, 'subenginecache.zssf8u.0001.use1.cache.amazonaws.com');
#define(REDIS_PORT, 6379);

date_default_timezone_set('Africa/Accra');


?>
