<?php

function request_callback($response, $info, $request, $extra) {

    #/*
    echo "info\n";
    #print_r($info);
    echo "url: ".$info['url']."\n";
    echo "http_code: ".$info['http_code']."\n";

    echo "response\n";
    $dlr_response = strip_n_cut($response, 16,TRUE);
    print_r($response)."\n";
    #print_r($dlr_response);

    echo "request:\n";
    print_r($request);
    #*/

    #$counter++;
    echo "\n\n NO OF REQ:".$extra['no_of_req']."\n\n";
    echo "\n\n COMPLETE REQ:".$extra['req_complete']."\n\n";
    $url_parts = url_to_key_values($info['url']);
    $url_dlr_parts = url_to_key_values($url_parts['dlr-url']);

    /*
      print_r($url_parts);

      print_r($url_dlr_parts);
     */


    $dlr_string = array();
    $dlr_string[] = $url_dlr_parts['content_date'] . " " . date('H:i:s');
    $dlr_string[] = $url_parts['from'];
    $dlr_string[] = $url_parts['to'];

    $trimmed_message = strip_n_cut($url_parts['text'], 16, TRUE);
    $dlr_string[] = $trimmed_message;

    $dlr_string[] = $url_dlr_parts['service_id'];

    $mno_detect = predict_network($url_parts['to']);
    $mno = $mno_detect['id'];
    $dlr_string[] = $mno;

    $dlr_string[] = 0; #type
    $dlr_string[] = $url_dlr_parts['content_id'];
    $dlr_string[] = $url_dlr_parts['msg_id'];
    $dlr_string[] = $info['http_code'];
    $dlr_string[] = BLASTER_TYPE;
    $dlr_string[] = $url_dlr_parts['blast_seq'];

    $dlr_string[] = $url_parts['smsc'];
    $dlr_string[] = ""; #dlr_submit
    $dlr_string[] = ""; #dlr_done
    $dlr_string[] = ""; #dlr_status
    $dlr_string[] = ""; #dlr_duration

    $dlr_string[] = $url_dlr_parts['sub_source'];

    /*
      $data_to_write = $now."\t".$from."\t".$to."\t".substr($msg,0,16)."\t".$service_id."\t".$smsc_id."\t".$type."\t";     #7
      $data_to_write .= $content_id."\t".$msg_id."\t".$http_status."\t".$blaster_id."\t".$blast_seq."\t";                  #5
      $data_to_write .= $dlr_smsc."\t".$dlr_submit."\t".$dlr_done."\t".$dlr_status."\t".$dlr_duration."\t".$sub_source."\n";
     */

    $dlr_string_output = implode("\t", $dlr_string);
    #echo $dlr_string_output . "\n";

    $time_taken = microtime(true) - $GLOBALS['time_start'];
    $speed = get_tps($time_taken, $extra['req_complete']);
    $percent = get_percent_complete($extra['req_complete'], $extra['no_of_req']);

    #CACHE PERECENTAGE AND OTHER PARAMS 
    #@cache_progress_bar_caller($percent);

    $messages_left = $extra['no_of_req'] - ($extra['req_complete'] + 1);
    $data = "[INFO],[CONTENT_DATE:" . $url_dlr_parts['content_date'] . "],";
    $data .= "[COUNT:" . ($extra['req_complete'] + 1) . " of {$extra['no_of_req']} = " . $percent . "%, TPS=" . $speed . "msg/s, will finish by " . get_finish_time($speed, $messages_left) . "],";
    $data .= "[NETWORK:" . $url_parts['smsc'] . " (" . $mno_detect['name'] . ")],";
    #$data .= "[COUNT:{$counter} of {$total_count}],";
    $data .= "[MSG:" . $trimmed_message . "],[RESPONSE:" . $info['http_code'] . " - " . $dlr_response . "],";
    $data .= "[SHORTCODE:" . $url_parts['from'] . "],[MSISDN:" . $url_parts['to'] . "],";
    $data .= "[SEQ:" . $url_dlr_parts['blast_seq'] . "],[CID:" . $url_dlr_parts['content_id'] . "],[SID:" . $url_dlr_parts['service_id'] . "],[SOURCE:" . $url_dlr_parts['sub_source'] . "]";

    debug($data);

    //PUT CODE CHECKING IF THE COUNT OF ARRAY $url_dlr_parts IS > X TO PREVENT WRITING CORRUPTED DATA
    #write_to_file($dlr_string_output, DLR_SENT_FILE);
}

/*
 * 
 * 
 * CALCULATING FUNCTIONS
 * 
 * 
 */

function get_tps($time, $messages) {
    $calculation = round($messages / $time);
    return $calculation;
}

function get_finish_time($speed, $messages_left) {
    $calculation = date('Y-m-d H:i:s', time() + round($messages_left / $speed));
    return $calculation;
}

function get_percent_complete($done, $total) {
    $calculation = round((($done + 1) / $total) * 100);
    return $calculation;
}

/*
 * 
 * 
 * PARSING FUNCTIONS
 * 
 * 
 */

function strip_n_cut($msg, $count, $strip) {
    #Strips out HTML and limits the text to a specific size

    if ($strip === TRUE) {
        $parsed1 = strip_tags($msg);
    }
    $parsed2 = str_replace(array("\t", "\n", "\r"), " ", $parsed1);
    $output = trim(substr($parsed2, 0, $count));
    return $output;
}

function url_to_key_values($url) {
    #Breaks down a URL to an Array of Key-Value Pairs

    $querystring = parse_url($url, PHP_URL_QUERY);
    #print_r($querystring);
    parse_str($querystring, $keyvalues);
    #print_r($keyvalues);
    return $keyvalues;
}

/*
 * 
 * FILE WRITERS
 * 
 */

function write_to_file($data, $file) {
    #CHECK THIS OUT
    #http://www.php.net/manual/en/function.stream-set-blocking.php
    $fp = fopen($file, 'a');
    #CONSIDER STRIPPING ALL NEWLINE ETC
    fwrite($fp, "{$data}\n");
}

function raw_url_log_file_name($date, $blaster_id, $blast_seq) {
    $types = $GLOBALS['BLASTER_TYPES'];
    $type = $types[$blaster_id];
    $hash = rand(1000, 9999);
    $str = "{$type}_raw_urls_{$date}_{$blaster_id}_{$hash}";
    return $str;
}

function log_file_name($date, $blaster_id) {
    $types = $GLOBALS['BLASTER_TYPES'];
    $type = $types[$blaster_id];
    $hash = rand(1000, 9999);
    $str = "{$type}_blaster-activity_{$date}_{$blaster_id}_{$hash}";
    return $str;
}

function bill_log_file_name($date, $blaster_id) {
    $types = $GLOBALS['BLASTER_TYPES'];
    $type = $types[$blaster_id];
    $hash = rand(1000, 9999);
    $str = "{$type}_billing-airtel_{$date}_{$blaster_id}_{$hash}";
    return $str;
}

function debug($text,$show=FALSE) {
    $output = show_now() . "::" . $text . "\n";
    
    if($show === FALSE){
        write_to_file($output, LOG_LOCATION . LOG_FILE_NAME);
    }else{
        echo $output;
    }
    
}

function debug_billing($text,$show=FALSE) {
    $output = show_now() . "::" . $text . "\n";

    if($show === FALSE){
        write_to_file($output, BILL_LOG_LOCATION . BILL_LOG_FILE_NAME);
    }else{
        echo $output;
    }

}

function dir_creator($path) {
    if (!is_dir($path)) {
        echo "Creating folder $path \n";
        $makedir = @mkdir($path, 0777, true);
        if ($makedir) {
            return true;
        } else {
            return false;
        }
    } else {
        echo "Folder already created $path \n";
        return true;
    }
}

function move_files($fullsource, $destinationdir) {
    $filename = basename($fullsource);
    $rename_n_move = rename($fullsource, $destinationdir . $filename);
    debug("Moving $fullsource => " . $destinationdir . $filename);

    if ($rename_n_move) {
        debug("Fresh Move complete!");
        return true;
    } else {
        debug("Cannot move files!");
        return false;
    }
}

/*
 * 
 * POSTGRES QUERY FUNCTIONS
 * 
 */

function sql_change($sql, $conn) {
    $query = pg_query($conn, $sql);

    if ($query) {
        $query_status = show_now() . "INFO: Query Ok\nQUERY=(\n$sql\n)\n";
        debug($query_status,TRUE);
        return $query;
    } else {
        $query_status = show_now() . "ERROR: Query Not Ok\nQUERY=(\n$sql\n)\nPG_ERROR=(" . pg_last_error($conn) . ")\n";
        debug($query_status);
        exit;
        return false;
    }
}

function sql_query($sql, $conn) {
    $query = pg_query($conn, $sql);
    $result = pg_fetch_assoc($query);
    if ($query) {
        $query_status = show_now() . "INFO: Query Ok\nQUERY=(\n$sql\n)\n";
        debug($query_status,TRUE);
        return $result;
    } else {
        $query_status = show_now() . "ERROR: Query Not Ok\nQUERY=(\n$sql\n)\nPG_ERROR=(" . pg_last_error($conn) . ")\n";
        error($query_status);
        exit;
        return false;
    }
}

function sql_query_all($sql, $conn) {
    $query = pg_query($conn, $sql);
    $result = pg_fetch_all($query);
    if ($query) {
        $query_status = show_now() . "INFO: Query Ok\nQUERY=(\n$sql\n)\n";
        debug($query_status,TRUE);
        return $result;
    } else {
        $query_status = show_now() . "ERROR: Query Not Ok\nQUERY=(\n$sql\n)\nPG_ERROR=(" . pg_last_error($conn) . ")\n";
        error($query_status);
        exit;
        return false;
    }
}

function get_blast_seq($blast_seq = NULL) {
    global $connection;
    if (!isset($blast_seq) || is_null($blast_seq)) {
        debug("Blast Sequence not Set. Setting it....");
        $sql = "SELECT nextval('blast_seq') as blast_seq;";
        $output = sql_query_all($sql, $connection);
        debug("BLAST SEQUENCE = " . $output[0]['blast_seq']);
        return $output[0]['blast_seq'];
    } else {
        debug("Blast Sequence Set to {$blast_seq}");
        return $blast_seq;
    }
}

function connect_to_db($conn_string) {
    $conn = pg_connect($conn_string);

    if ($conn) {     
        $conn_status = show_now() . "INFO: Database Connection Ok";
        debug($conn_status);
        return $conn;
    } else {
        $conn_status = show_now() . "ERROR: Database Connection Failed\nPG_ERROR=(" . pg_last_error($conn) . ")\n";
        debug($conn_status);
        exit;
        return false;
    }
}

/*
 * 
 * TIME FUNCTIONS
 * 
 */

function show_now() {

    return "" . date("Y-m-d H:i:s") . " ";
}

function show_now_file_name() {
    
    return date("Ymd_His");
}

/*
 * 
 * CACHING FUNCTIONS
 * 
 *

function cache_progress_bar_caller($new) {
    global $old, $redis;

    #echo "new = $new, old = $old \n";

    if ($new != $old) {
        $old = $new;

        #echo "\n\n REDIS NAME = " . $GLOBALS['REDIS_NAME'] . ".\n\n";
        @$redis->set($GLOBALS['REDIS_NAME'], $new);
        $timenow = time(NULL); // current timestamp
        @$redis->expireAt($GLOBALS['REDIS_NAME'], $timenow + (24 * 60 * 60)); #will expire in 24 hours
        #echo "FETCHED:" . $redis->get($GLOBALS['REDIS_NAME']) . "\n\n";
        #echo "==Alert old has just been set to $old == \n";
    }
}

/*
 * 
 * BROADCAST FUNCTIONS
 * 
 */
function billuser($row){
	#Bill Function for Sierra Leone
	#$url = "http://50.19.73.204/charge_gateway_ws/index.php?msisdn=23276486316&sid=1&shortcode=771&keywd=News&p=450.0";
	#$url = "http://50.19.73.204/charge_gateway_ws/index.php?msisdn=[MSISDN]&sid=1&shortcode=771&keywd=[KEYWORD]&p=450.0";

    $find = array('+', '+233', '233');
	$from = $row['shortcode'];
    $to = trim(str_replace($find,'',$row['msisdn']));
    $transID = $from . time();

	$url = "http://localhost:8083/index.php?msisdn={$to}&transID={$transID}";

	$ch = curl_init($url);

	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$response = curl_exec($ch);
	#var_dump($url);
	#var_dump($response);
	curl_close($ch);

	$doc = new DOMDocument;
	$doc->loadXML($response);

	$status = $doc->getElementsByTagName('value')->item(1)->nodeValue;
	$now = date("Y-m-d H:i:s");
	$logurl = str_replace(array("\t", "\n", "\r"), " ", $url);
	#$logresponse = str_replace(array("\t", "\n", "\r"), " ", $response);
	write_to_file("$now\t$status\t$logurl",'/opt/kannel/logs/billing.log');
	if( ($status == 0 ) || ($status == 1 ) || ($status == 1 ) ){
		#Success
		return TRUE;
	}else{
		#Failure
		return FALSE;
	}
}

function sendsms($row, $extra = NULL) {
	
	
    $from = $row['shortcode'];
    $to = $row['msisdn'];
    #CLEAN SOURCE & DESTINATION

    $cid = $row['content_id'];
    $sid = $row['service_id'];
    $msg = urlencode(str_replace("\t", " ", $row['message']));
    $sub_source = urlencode($row['sub_source']);

    $msgid = uniqid();

    if ($row['blaster_id']) {
        if ($row['blaster_id'] < 10) {
            $blasterid = 10;
        } else {
            $blasterid = $row['blaster_id'] + 1;
        }
    } else {
        $blasterid = BLASTER_TYPE;
    }



    #$blast_seq = $extra['blast_seq'];
    $blast_seq = BLAST_SEQ;

    $valid = urlencode($row['valid_date']);
    
    #TRUNCATE SINFO / ACCOUNT TO 64 CHARS OR LESS
    $trunclimit = 63;
    $sinfo = urlencode(substr("xseq:{$blast_seq}&xsid:{$sid}&xcid:{$cid}&xbid:{$blasterid}",0,$trunclimit));

    #LOGIC: TO BE DETERMINED
    $routing_details = get_host_port_smsc_network($to, $from);
    $host = $routing_details['host'];
    $port = $routing_details['port'];
    $smsc = $routing_details['smsc'];
    #$network = $routing_details['network'];

    $host_dlr = "localhost";
    $port_dlr = ":8082";
    $host_port_dlr = "{$host_dlr}{$port_dlr}";

    $dlrmask = DLR_MASK;

    #$dlr_url = "http://{$host_port_dlr}/360/dlrkiller/write.php?to=%p&from=%P&msg=%a";
    $dlr_url = "http://{$host_port_dlr}/write.php?to=%p&from=%P&msg=%a";
    $dlr_url .= "&type=%d&dlr_smsc=%i&service_id={$sid}&blaster_id={$blasterid}&content_date={$valid}";
    $dlr_url .= "&content_id={$cid}&msg_id={$msgid}&blast_seq={$blast_seq}&sub_source={$sub_source}";

    $url = "http://{$host}{$port}/cgi-bin/sendsms?";
    $url .= "username=mtech&password=mt3ch&from=$from&to=$to&text=$msg";
    $url .= "&dlr-mask=$dlrmask&dlr-url=" . urlencode($dlr_url) . "&priority=0{$smsc}&account={$sinfo}";
    
    //#Added by Demola for UAT
    //$nosilent = array(385,386,387,388,389);
    //
    //#Added by Demola for UAT
    //if (($cid == 0 && !in_array($cid,$nosilent)) || BLASTER_TYPE == REBLASTER) {
    //    #SILENT SMS
    //    $url .= "&pid=64&mclass=0";
    //}
	
	#TRY TO BILL AIRTEL USER FIRST BEFORE SENDING MESSAGE
    //if ($routing_details['name'] == 'GHA_AIRTEL') {
    //    if (billuser($row) === FALSE) {
    //        $url = "http://127.0.0.1:8083/505.html";
    //        $billing_data = $row['shortcode'] . "\t" . $row['msisdn'] . "\t" . $row['content_id'] . "\t" . $row['service_id'] ."\n";
    //        debug_billing($billing_data);
    //    }
    //}
	
    #echo $url;
    debug($url);
    return $url;
}

/*
 * 
 * NETWORK PREFIX FUNCTIONS
 * 
 */

function predict_network($number) {
    $mno_array = array(
        0 =>  array('name' => "NONE", 'regex' => "^.+"),
        1 =>  array('name' => "NIG_GLO", 'regex' => "^(\+?)(0|234)?(705|805|807|815|811|905)"),
        2 =>  array('name' => "NIG_AIRTEL", 'regex' => "^(\+?)(0|234)?(808|708|802|812|701|902)"),
        3 =>  array('name' => "NIG_MTN", 'regex' => "^(\+?)(0|234)?(703|706|803|806|810|813|814|816|903)"),
        4 =>  array('name' => "NIG_ETISALAT", 'regex' => "^(\+?)(0|234)?(809|817|818|909)"),
        5 =>  array('name' => "NIG_MULTILINKS", 'regex' => "^(\+?)(0|234)?(77|017|177|7027|709|1017|1027|109|1031|1039)"),
        6 =>  array('name' => "NIG_STARCOMMS", 'regex' => "^(\+?)(0|234)?(180|181|185|187|189|64|74|84|7028|7029|819)"),
        7 =>  array('name' => "NIG_VISAFONE", 'regex' => "^(\+?)(0|234)?(7025|7026|704)"),
        7 =>  array('name' => "NIG_MTEL", 'regex' => "^(\+?)(0|234)?(804)"),
        7 =>  array('name' => "NIG_ZOOM_RELTEL", 'regex' => "^(\+?)(0|234)?(707)"),
        8 =>  array('name' => "GHA_VODAFONE", 'regex' => "^(\+?)(0|233)?(20|50)"),
        9 =>  array('name' => "GHA_TIGO", 'regex' => "^(\+?)(0|233)?(27|57)"),
        10 => array('name' => "GHA_EXPRESSO", 'regex' => "^(\+?)(0|233)?(28)"),
        11 => array('name' => "GHA_MTN", 'regex' => "^(\+?)(0|233)?(24|54|55)"),
        12 => array('name' => "GHA_AIRTEL", 'regex' => "^(\+?)(0|233)?(26)"),
        13 => array('name' => "GHA_GLO", 'regex' => "^(\+?)(023|23323)")
    );

    foreach ($mno_array as $key => $value) {
        if (preg_match("/" . $value['regex'] . "/", $number)) {
            $mno = array('id' => $key, 'name' => $value['name']);
        }
    }

    if (empty($mno)) {
        $mno = array('id' => 0, 'name' => "NONE");
    }
    return $mno;
}

function get_host_port_smsc_network($to, $from) {
    $network = predict_network($to);
    switch ($network['id']) {

        #GHA_VODAFONE
        case 8:
            $host = "localhost";
            $port = ":14031";
            $smscid = 'VODAFONE';
            $smsc = "&smsc={$smscid}";

        break;
        #GHA_TIGO
        case 9:
            $host = "localhost";
            $port = ":14031";
            $smscid = '';
            $smsc = "";
        break;
        #GHA_ESPRESSO
        case 10:
            $host = "localhost";
            $port = ":14031";
            $smscid = '';
            $smsc = "";
        break;
        #GHA_AIRTEL
        case 12:
            $host = "localhost";
            $port = ":14031";
            $smscid = '';
            $smsc = "";
        break;
        #GHA_GLO
        case 13:
            $host = "localhost";
            $port = ":14031";
            $smscid = 'GLOBACOMM';
            $smsc = "&smsc={$smscid}";
        break;

        #UNKNOWN
        default:
        $host = "localhost";
        $port = ":14031";
        $smscid = '';
        $smsc = "";
        break;
    }

    return array('host' => $host, 'port' => $port, 'smsc' => $smsc, 'network_id' => $network['id'], 'network' => $network['name']);
}

?>
