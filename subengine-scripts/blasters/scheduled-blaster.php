<?php
error_reporting(0);
$rank = trim($argv[1]);
require_once("blaster-config.php");
require_once("blaster-functions.php");
require_once("LiveRollingCurlBlaster.php");

#START-UP PROCEDURE
dir_creator(LOG_LOCATION);
dir_creator(BILL_LOG_LOCATION);
dir_creator(URL_LOG_LOCATION_SCHEDULED);
dir_creator(URL_LOG_BACKUP);
dir_creator(DLR_SENT_LOCATION);

define(BLASTER_TYPE,BLASTER_SCHEDULED);
define(LOCATION,URL_LOG_LOCATION_SCHEDULED);
define(LOG_FILE_NAME,log_file_name(date("Ymd"), BLASTER_TYPE).".log");
define(BILL_LOG_FILE_NAME,bill_log_file_name(date("Ymd"), BLASTER_TYPE).".log");

#CONNECT TO DATABASE
$connection = connect_to_db(CONN_GHANA);

/*
 * 
 * To reblast 
 * remove detector
 * comment get_blast_seq function
 * add $blast_seq = your blast id
 * change query to reflect the id that was last blasted and point to table
 * restore to original / revert back to original once the blast starts 
 * git push and git pull
 */

#SCRIPT TO DETECT IF CONTENT EXISTS FOR SCHEDULED BLAST
#require_once("scheduled-detect.php");

#GET BLAST_SEQ IF IT DOESNT EXIST
#$blast_seq = 7414;
$blast_seq = get_blast_seq();
define(BLAST_SEQ,$blast_seq);


$tablename = str_replace('-', '_', $GLOBALS['BLASTER_TYPES'][BLASTER_TYPE]);
$temp_table = "blast.blast_details_{$tablename}_".BLAST_SEQ;
debug("Blast Table name is ".$temp_table);

$sql_blast = "
--CREATE TABLE with serial id

CREATE TABLE {$temp_table}
(
  blast_id serial NOT NULL,
  
  --content
  content_id integer NOT NULL,
  valid_date timestamp without time zone,
  message character varying(200) NOT NULL,
  service_id bigint NOT NULL,
  
  --services
  service_period character varying,
  shortcode character varying,
  max_content integer NOT NULL DEFAULT 1,
  
  --subscription details
  msisdn character varying,
  sub_source character varying
  
  --creation_time timestamp without time zone DEFAULT now(),
)
WITH (
  OIDS=FALSE
);

--SELECT DATA & INSERT DATA INTO TABLE
INSERT INTO {$temp_table}(content_id, valid_date, message, service_id, service_period,
            shortcode, max_content, msisdn, sub_source)
(
  select
      a.id as content_id,
      a.pubdate as valid_date,
      a.description as message,
      a.cat_id as service_id,
      ss.service_period as service_period,
      ss.service_shortcode as shortcode,
      ss.delivery_per_day as max_content,
      s.sub_msisdn as msisdn,
      s.sub_source
  from
      subscription s,
      ( SELECT cat_id, pubdate, description, id, rank() OVER (partition BY cat_id ORDER BY id)
      FROM mtechghana2.content_messages cm
      WHERE pubdate = now()::date
      AND cat_id NOT IN (11269, 11270, 1273)
      ) a,
      subscription_service ss
  where
      s.sub_service_id = a.cat_id         --join subscribers service id with services id
      --and s.sub_status = 'active'                    --only active subscribers
      and ss.is_active = 1                   --only active services
      and ss.id = a.cat_id            --join content service id with services id
      and s.sub_network NOT IN ('AIRTEL','TIGO')

      and s.sub_service_id NOT IN (11269, 11270, 1273)
      and a.rank = {$rank}

      and a.id not in (select content_id from blast_summary where date::date = now()::date)
      and a.pubdate = now()::date     --if date is the same
);	

--INSERT SUMMARIZED DATA INTO BLAST SUMMARY
INSERT INTO blast_summary(seq, subs, content_id, service_id, service, shortcode, message)
(
  select
          currval('blast_seq'),
          count(msisdn),
          a.content_id,
          a.service_id,
          sx.service_name as service,
          a.shortcode,
          a.message
  from
          {$temp_table} a,
          subscription_service sx
  where
  sx.id = a.service_id
  group by
          a.content_id,
          a.service_id,
          sx.service_name,
          a.shortcode,
          a.message
  order by
  a.service_id
);

--SELECT DATA BY ORDER AND SHOW 
  select
      b.blast_id,
      b.content_id,
      b.valid_date,
      b.message,
      b.service_id,
      b.shortcode,
      b.msisdn,
      b.sub_source
  from
          {$temp_table} b
  order by 
      b.blast_id, b.msisdn, b.service_id
  ;
";
#$sql_blast = "select * from _newtired_ limit 5;";
#$sql_blast = "select * from {$temp_table} limit 5;";

#$sql_blast = "select * from blast_details_instant_7414 where blast_id > 1273325 order by blast_id";


                                                     
debug($sql_blast);
$time_start = microtime(true);

$result = pg_query($connection, $sql_blast);

$time_end = microtime(true);
$time = $time_end - $time_start;
$testresult1 = "Query Completed in {$time} seconds \n";
sleep(5);
debug($testresult1);
debug(pg_last_error());



#START PROGRESS BAR CACHE
#$redis = new Redis();
#$redis->connect(REDIS_HOST, REDIS_PORT);
#$GLOBALS['REDIS_NAME'] = "progress_bar_" . date("Ymd") . "_" . $tablename;
#echo "\n\n REDIS NAME = " . $GLOBALS['REDIS_NAME'] . ".\n\n";






$time_start = microtime(true);
$GLOBALS['time_start'] = $time_start;
$counter = 0;
$total_count = pg_num_rows($result);
#############################################
#############################################
$rc = new RollingCurl("request_callback");
$rc->window_size = CONNECTIONS; 
$rc->execute();
#############################################
#############################################
$time_end = microtime(true);
$time = $time_end - $time_start;

$testresult2 = "Completed {$total_count} calls in {$time} seconds OR ".(round($time/60))." mins ( @ ".(get_tps($time,$total_count))." msgs/s with Connections:  ".CONNECTIONS.")\n";
debug($testresult2);

#CLOSE PROGRESS BAR CACHE
#$redis->close();

debug("Removing Log File!");
move_files(LOG_LOCATION.LOG_FILE_NAME, URL_LOG_BACKUP);
debug("Completely finished!");
unlink(URL_LOG_BACKUP.LOG_FILE_NAME);
?>
