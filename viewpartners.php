<?php

  
$page_title = "view partners";
$section = "Schools";   

include ("header.php");
  if($_SESSION['access_level'] != "5"){
    header('location:index.php');
}
?>


 <div id="modal" class="modal fade"
     tabindex="-1" role="dialog" aria-labelledby="plan-info" aria-hidden="true">
    <div class="modal-dialog modal-full-screen">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                  data-dismiss="modal" aria-hidden="true">
                  <span class="glyphicon glyphicon-remove-circle"></span>
                </button>
            </div>
            <div class="modal-body">
                <!-- /# content goes here -->
            </div>
        </div>
    </div>
</div>
        <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div id="ajax-modal"></div>

            <div class="page-content">
            
                    <?php include_once('includes/breadcrumbs/breadcrumbs.php'); ?>
                    
            <div class="clearfix"></div>
            <!-- END DASHBOARD STATS 1-->
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-globe"></i>All <?php echo ucwords($section) ?> </div>
                            <div class="tools"> </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover" id="view_partners" width="100%">
                                <thead>
                                    <tr>
                                        <th> SCHOOL NAME</th>
                                        <th> EVENT TYPE</th>
                                        <th> CONTACT NO. </th>
                                        <th> ACTION</th>   
                                         
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
							
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<!-- Modal -->

      
<?php   
 
include ("footer.php");
?>
